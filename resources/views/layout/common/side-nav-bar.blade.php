<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="@if(Request::segment(1) == 'dashboard') active @endif"><a href="{{ route('dashboard') }}"><i class="fa fa-home"></i> <span>Dashboard</span>
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
        </li>
        <li class="@if(Request::segment(1) == 'products') active @endif"><a href="{{ route('products.index') }}"><i class="fa fa-product-hunt" aria-hidden="true"></i> <span> Products </span> </a></li>
        <li class="treeview @if(Request::segment(1) == 'umpdefaults' || Request::segment(1) == 'manage-title' || Request::segment(1) == 'manufacturer' || Request::segment(1) == 'condition') active @endif">
            <a href="#"><i class="fa fa-crosshairs"></i> <span> Master </span> </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('umpdefaults') }}"><i class="fa fa-eye"></i>Defaults</a></li>
                <li><a href="{{ route('products.managetitle.get') }}"><i class="fa fa-flask"></i>Manage Title</a></li>
                <li><a href="{{ route('manufacturer.index') }}"><i class="fa fa-compass"></i>Manufacturers</a></li>
                <li><a href="{{ route('condition.index') }}"><i class="fa fa-deaf"></i>Conditions</a></li>
            </ul>
        </li>
        <li class="treeview @if(Request::segment(1) == 'google') active @endif">
            <a href="{{ route('google.dashboard.get') }}"><i class="fa fa-google" aria-hidden="true"></i> <span> Google Shopping </span></a>
            <ul class="treeview-menu">
                <li><a href="{{ route('googledefaults') }}"><i class="fa fa-asterisk"></i> Defaults</a></li>
                <li><a href="{{ route('google.mapcondition.get') }}"> <i class="fa fa-sliders" aria-hidden="true"></i> Global Setting</a></li>
                <li><a href="{{ route('google.blockproduct.get') }}"> <i class="fa fa-puzzle-piece" aria-hidden="true"></i> Product Setting</a></li>
                <li><a href="{{ route('google.batch.get') }}"><i class="fa fa-cloud-upload" aria-hidden="true"></i> Batch Upload</a></li>
                <li><a href="{{ route('google.suspendaccount.get') }}"><i class="fa fa-ban"></i> Account Suspension</a></li>
            </ul>
        </li>
        <li class="treeview @if(Request::segment(1) == 'shopping') active @endif">
            <a href="{{ route('shopping.dashboard.get') }}"><i class="fa fa-shopping-bag"></i>  <span> Shopping.com </span> </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('shoppingdefaults.get') }}"><i class="fa fa-asterisk"></i>Defaults</a></li>
                <li><a href="{{ route('shopping.mapcondition.get') }}"> <i class="fa fa-sliders" aria-hidden="true"></i> Global Setting</a></li>
                <li><a href="{{ route('shopping.blockproduct.get') }}"> <i class="fa fa-puzzle-piece" aria-hidden="true"></i> Product Setting</a></li>
                <li><a href="{{ route('shopping.batch.get') }}"><i class="fa fa-cloud-upload" aria-hidden="true"></i> Batch Upload</a></li>
                <li><a href="{{ route('shopping.suspendaccount.get') }}"><i class="fa fa-ban"></i>Account Suspension</a></li>
            </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>