<ul class="nav nav-tabs">
    <li class="{{$active=='price'?'active':''}}">
        <a href="{{ route('ebay.pricearrangement.get') }}">
            Price Arrangement
        </a>
    </li>
    <li class="{{$active=='quantity'?'active':''}}">
        <a href="javascript:void(0)">
            Quantity Arrangement
        </a>
    </li>
    <li class="{{$active=='block'?'active':''}}">
        <a href="{{ route('ebay.blockproduct.get') }}">
            Blocked Products
        </a>
    </li>
    <li class="{{$active=='category'?'active':''}}">
        <a href="{{ route('ebay.productmapcategory.get') }}">
            Product Category Mapping
        </a>
    </li>
    <li class="{{$active=='condition'?'active':''}}">
        <a href="{{ route('ebay.mapproductcondition.get') }}">
            Product Condition Mapping
        </a>
    </li>
</ul>