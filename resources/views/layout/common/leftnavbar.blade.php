<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
                </a>
            </li>
            <li class="treeview @if(Request::segment(1) == 'products') active @endif">
                <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Products</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="#">
                            <i class="fa fa-files-o"></i>
                            <span>Master</span>
                            <i class="fa fa-angle-left pull-right"></i>
                            <ul class="treeview-menu">
                        <a href="{{route('manufacturer.index')}}"><i class="fa fa-circle-o"></i> Manufacturers</a>
                        </ul>
                        <ul class="treeview-menu">
                            <a href="{{route('condition.index')}}"><i class="fa fa-circle-o"></i> Conditions</a>
                        </ul>
                        </a>
                    </li>
                    <li class="treeview @if(Request::segment(2) == 'create') active @endif"><a href="{{URL::route('products.create')}}"><i class="fa fa-circle-o"></i> Create</a></li>
                    <li class="treeview @if(Request::segment(1) == 'products' && Request::segment(2) == '') active @endif"><a href="{{URL::route('products.index')}}"><i class="fa fa-circle-o"></i> All Products</a></li>
                </ul>
            </li>
            <li class="treeview @if(Request::segment(1) == 'ebaydefaults') active @endif">
                <a href="{{URL::route('ebaydefaults.create')}}">
                <i class="fa fa-files-o"></i>
                <span>Ebay Defaults</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Old Products</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::route('universal_product.create')}}"><i class="fa fa-circle-o"></i> Create</a></li>
                    <li><a href="{{URL::route('universal_product.index')}}"><i class="fa fa-circle-o"></i> All Products</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                <i class="fa fa-files-o"></i>
                <span>User</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::route('user.products.get')}}"><i class="fa fa-circle-o"></i> Products</a></li>
                    <li><a href="{{URL::route('ebay.mapcategory.get')}}"><i class="fa fa-circle-o"></i> Change Category</a></li>
                    <li><a href="{{URL::route('excelexportpage.get')}}"><i class="fa fa-circle-o"></i> Excel Export</a></li>
                    <li><a href="{{URL::route('uploadexcel.get')}}"><i class="fa fa-circle-o"></i> Excel Import</a></li>
                    <li><a href="{{URL::route('product.upload.ebay')}}"><i class="fa fa-circle-o"></i> Ebay Upload</a></li>
                </ul>
            </li>
        </ul>
    </section>
</aside>