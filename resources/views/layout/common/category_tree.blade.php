<div class="box box-success">
    <div class="box-header with-border">
        <i aria-hidden="true" class="fa fa-search-plus">
        </i>
        <h3 class="box-title">
        Select Multiple Category
        </h3>
    </div>
    <div class="box-body">
        <div class="form-group required">
            <label class="col-sm-3 control-label">
                Category :-
            </label>
            <div class="col-sm-7">
                <div class="form-group">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <input class="form-control col-xs-12" id="search-input" placeholder="Search Categories"/>
                            <span class="input-group-addon">
                                <i class="fa fa-search">
                                </i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-6 selectAll">
                        <div class="input-group">
                            <button class="btn btn-xs btn-success selectAllCat" type="button">
                            <i class="fa fa-check-circle-o" aria-hidden="true"></i> SelectAll
                            </button>
                        </div>
                    </div>
                    <div class="col-lg-6 hide deSelectAll">
                        <div class="input-group">
                            <button class="btn btn-xs btn-danger deSeletAllCat" type="button">
                            <i class="fa fa-check-circle-o" aria-hidden="true"></i> DeSelectAll
                            </button>
                        </div>
                    </div>
                </div>
                <div class="jstree-scroll">
                    <div class="row">
                        <div class="col-md-6">
                            <div id="jsEvenTree">
                                @foreach($categoryEvenTree as $firstLevelCat => $secondLevelCatArr)
                                    <ul>
                                        <li id="{{ $firstLevelCat }}">
                                            {{ $allCategory[$firstLevelCat] }} [{{ isset($productsInCategory[$firstLevelCat]) ? $productsInCategory[$firstLevelCat] : 0 }}]
                                            <ul>
                                                @foreach($secondLevelCatArr as $secondLevelCat => $thirdLevelCatArr )
                                                @if(isset($allCategory[$secondLevelCat]))
                                                <li id="{{ $secondLevelCat }}">
                                                    {{ $allCategory[$secondLevelCat] }} [{{ isset($productsInCategory[$secondLevelCat]) ? $productsInCategory[$secondLevelCat] : 0  }}]
                                                    <ul>
                                                        @foreach($thirdLevelCatArr as $thirdLevelCat)
                                                        @if(isset($allCategory[$thirdLevelCat]))
                                                        <li id="{{ $thirdLevelCat }}">
                                                            {{ $allCategory[$thirdLevelCat] }} [{{ isset($productsInCategory[$thirdLevelCat]) ? $productsInCategory[$thirdLevelCat] : 0  }}]
                                                        </li>
                                                        @endif
                                                        @endforeach
                                                    </ul>
                                                </li>
                                                @endif
                                                @endforeach
                                            </ul>
                                        </li>
                                    </ul>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div id="jsOddTree">
                                @foreach($categoryOddTree as $firstLevelCat => $secondLevelCatArr)
                                <ul>
                                    <li id="{{ $firstLevelCat }}">
                                        {{ $allCategory[$firstLevelCat] }} [{{ isset($productsInCategory[$firstLevelCat]) ? $productsInCategory[$firstLevelCat] : 0 }}]
                                        <ul>
                                            @foreach($secondLevelCatArr as $secondLevelCat => $thirdLevelCatArr )
                                            @if(isset($allCategory[$secondLevelCat]))
                                            <li id="{{ $secondLevelCat }}">
                                                {{ $allCategory[$secondLevelCat] }} [{{ isset($productsInCategory[$secondLevelCat]) ? $productsInCategory[$secondLevelCat] : 0  }}]
                                                <ul>
                                                    @foreach($thirdLevelCatArr as $thirdLevelCat)
                                                    @if(isset($allCategory[$thirdLevelCat]))
                                                    <li id="{{ $thirdLevelCat }}">
                                                        {{ $allCategory[$thirdLevelCat] }} [{{ isset($productsInCategory[$thirdLevelCat]) ? $productsInCategory[$thirdLevelCat] : 0  }}]
                                                    </li>
                                                    @endif
                                                    @endforeach
                                                </ul>
                                            </li>
                                            @endif
                                            @endforeach
                                        </ul>
                                    </li>
                                </ul>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <span class="thumbnail text-center" style="background-color:#00a65a;color:#FFFFFF">
                    <b id="remain_category_count" style="font-size:30px">
                    0
                    </b>
                    <br>
                    Category selected
                    </br>
                </span>
            </div>
        </div>
    </div>
    <div class="overlay" id="search_spin" style="display:none;">
        <i class="fa fa-spin fa-spinner">
        </i>
    </div>
</div>