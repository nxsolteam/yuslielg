<header class="main-header">
    <nav class="navbar navbar-static-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">
                    To Do List
                </a>
                <button class="navbar-toggle collapsed" data-target="#navbar-collapse" data-toggle="collapse" type="button">
                    <i class="fa fa-bars">
                    </i>
                </button>
            </div>
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="@if(Request::segment(1) == 'dashboard') active @endif">
                        <a href="{{ route('dashboard') }}">
                            <i class="fa fa-home">
                            </i>
                            Dashboard
                        </a>
                    </li>
                    <li class="@if(Request::segment(1) == 'group') active @endif">
                        <a href="{{ route('group.index') }}">
                            <i class="fa fa-group">
                            </i>
                            Group
                        </a>
                    </li>
                    <li class="@if(Request::segment(1) == 'task') active @endif">
                        <a href="{{ route('task.index') }}">
                            <i class="fa fa-list">
                            </i>
                            Task
                        </a>
                    </li>
                    <li class="@if(Request::segment(1) == 'taskUser') active @endif">
                        <a href="{{ route('taskUser.index') }}">
                            <i class="fa fa-user">
                            </i>
                            User
                        </a>
                    </li>
                    <li class="@if(Request::segment(1) == 'taskAssign') active @endif">
                        <a href="{{ route('taskAssign') }}">
                            <i class="fa fa-user">
                            </i>
                            Assign Task To Group
                        </a>
                    </li>
                    <li class="@if(Request::segment(1) == 'groupAssign') active @endif">
                        <a href="{{ route('groupAssign') }}">
                            <i class="fa fa-user">
                            </i>
                            Assign User To Group
                        </a>
                    </li>
                </ul>
            </div>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="glyphicon glyphicon-user">
                            </i>
                            <span>
                                {{ ucfirst($userName) }}
                                <i class="caret">
                                </i>
                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li role="presentation">
                                <a href="{{ route('changepassword.get') }}" role="menuitem" tabindex="-1">
                                    <i class="fa fa-lock">
                                    </i>
                                    Change Password
                                </a>
                            </li>
                            <li class="divider" role="presentation">
                            </li>
                            <li role="presentation">
                                <a href="{{ route('logout') }}" role="menuitem" tabindex="-1">
                                    <i class="fa fa-power-off">
                                    </i>
                                    Logout
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>

