<!-- Delete Confirm -->
<div aria-hidden="true" aria-labelledby="confirmDeleteLabel" class="modal fade" id="confirmDelete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                    ×
                </button>
                <h4 class="modal-title" style="text-transform: capitalize;">
                </h4>
            </div>
            <div class="modal-body" style="text-transform: capitalize;">
                <p>
                </p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" type="button">
                    <i class="fa fa-times-circle-o" aria-hidden="true"></i> Cancel
                </button>
                <button class="btn btn-danger model_click" id="confirm" type="submit">
                    <i class='fa fa-fw fa-trash-o fa-lg'></i> Delete
                </button>
            </div>
        </div>
    </div>
</div>
<!-- Delete Confirm -->


<!-- Replace Confirm -->
<div aria-hidden="true" class="modal fade" id="replaceConfirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                    ×
                </button>
                <h4 class="modal-title" style="text-transform: capitalize;">
                </h4>
            </div>
            <div class="modal-body">
                
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" type="button">
                    Cancel
                </button>
                <button class="btn btn-success model_click" id="replace" type="submit">
                    Assign
                </button>
            </div>
            <div class="overlay" id="modal_spin" style="display:none;">
                <i class="fa fa-spin fa-spinner">
                </i>
            </div>
        </div>
    </div>
</div>
<!-- Replace Confirm -->
<style type="text/css">
#replaceConfirm .modal-dialog {
    height: 700px;
    width: 700px;
}

#modal_support .modal-dialog {
    height: 800px;
    width: 800px;
}
</style>

<!-- Support Modal -->
<div class="modal" data-backdrop="static" id="modal_support">
    <div class="modal-dialog">
        <div class="modal-content box">
            <form class="form-horizontal" id="frm-add_language">
                <div class="modal-header">
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                    <h4 class="modal-title">
                        Need Support? Please fill the form below and we will get in touch with you.
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="callout callout-success" id="modal_map_success" style="display:none;">
                        <p>
                            We have received your support request,<br>
                            We will contact you soon on your registered email id.
                        </p>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Subject :-</label>
                        <div class="col-sm-10">
                            {!! Form::text('subject',Input::get('subject',null),array('class'=>'form-control subject','placeholder'=>'Subject')) !!}
                            <span class="help-inline text-danger" id="subject_error">
                            </span>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Message :- </label>
                        <div class="col-sm-10">
                            {!! Form::textarea('message',Input::get('message',null),array('class'=>'form-control message','placeholder' => 'Message')) !!}
                            <span class="help-inline text-danger" id="message_error">
                            </span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success btnSupport" id="global_map_category" type="submit">
                        Send
                    </button>
                    <button class="btn btn-default btn_support_reset" id="btn_support_reset" type="reset">
                        Cancel
                    </button>
                </div>
            </form>
            <div class="overlay" id="modal_spin" style="display:none;">
                <i class="fa fa-spin fa-spinner">
                </i>
            </div>
        </div>
    </div>
</div>
<!-- Support Modal End-->