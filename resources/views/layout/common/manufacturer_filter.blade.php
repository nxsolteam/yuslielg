<div class="box box-success">
    <div class="box-header with-border">
        <i aria-hidden="true" class="fa fa-search-plus">
        </i>
        <h3 class="box-title">
            Manufacturer Search
        </h3>
    </div>
    <div class="box-body">
        <form class="form-horizontal" id="frm_filter" name="frm_filter" role="form">
            <div class="form-group required">
                <label class="col-sm-2 control-label">
                    Manufacturer :-
                </label>
                <div class="col-sm-6">
                    {{ Form::select('manufacturer',$manufacturers,Input::old('manufacturer'),array('id' => 'manufacturer','class'=>'form-control select2 input-sm local_manufacturer_search')) }}
                </div>
            </div>
            <div class="col-md-offset-2">
                <button class="btn btn-sm btn-success" type="submit">
                    Search
                </button>
                <button class="btn btn-sm btn-default" id="btn_reset" type="reset">
                    Reset
                </button>
            </div>
        </form>
    </div>
    <div class="overlay" id="search_spin" style="display:none;">
        <i class="fa fa-spin fa-spinner">
        </i>
    </div>
</div>