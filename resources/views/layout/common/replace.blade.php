<p>You have <b>{{$countProduct}}</b> number of products with this <b>{{$typeName}}</b> {{$type}}. Which {{$type}} would you like to assign to them?</p>
<br/>
{{ Form::hidden("typeId",$ids,array("id" => "typeId")) }}
<div class="form-group required">
    <label class="col-sm-4 control-label">
        Assign {{ucfirst($type)}}:-
    </label>
    <div class="col-sm-8" >
        {{ Form::select("replaceId",$typeList,Input::get("replaceId",null),array("class"=>"form-control","id" => "replaceId")) }}
        <span class="help-inline text-danger" id="replaceId_error">
        </span>
    </div>
</div>
<Br>
