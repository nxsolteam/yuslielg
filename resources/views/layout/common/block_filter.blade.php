<div class="box box-success">
    <div class="box-header with-border">
        <i aria-hidden="true" class="fa fa-search-plus">
        </i>
        <h3 class="box-title">
            Product Search By Product {{ucfirst($keyword)}} 
        </h3>
        <!-- /. tools -->
    </div>
    <form class="form-horizontal" id="frm_filter" name="frm_filter" role="form">
        <div class="box-body">
            <div class="row">
                <div class="col-sm-6">
                    @if(isset($isPrice) && $isPrice == 'Yes')
                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Price :-</label>
                            <div class="col-sm-4">
                                {!! Form::select('price_filter',['>' => 'Greater Then (>)','>=' => 'Greater Then OR Equal To (>=)','<' => 'Less Then','<=' => 'Less Then OR Equal To (<=) ','=' => 'Equal To (=)','!=' => 'Not Equal To (!=)' ],Input::old('price_filter'),array('class'=>'form-control','id'=>'price_filter')) !!}
                            </div>
                            <div class="col-sm-4">
                                {!! Form::text('price_filter_value',Input::old('price_filter_value'),array('class'=>'form-control numeric','placeholder'=>"Search Product By $keyword",'id'=>'price_filter_value')) !!}
                            </div>
                        </div>
                    @else
                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Keyword :-</label>
                            <div class="col-sm-8">
                                {!! Form::text('block_filter',Input::old('block_filter',null),array('class'=>'form-control','placeholder'=>'Search Product By Title,Short Description,Description and Keyword','id'=>'block_filter')) !!}
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            Category :- 
                        </label>
                        <div class="col-sm-8" id="divlocalchildcat">
                            {!! Form::select('category',[],Input::old('category'),array('id' => 'main_category','class'=>'form-control input-sm local_category_search')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            Distributor :- 
                        </label>
                        <div class="col-sm-8" id="divlocalchildcat">
                            {!! Form::select('vendor',$distributor,Input::old('vendor'),array('class'=>'form-control input-sm vendorChange select2','data-bind' =>'block')) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="form-group">
                <div class="col-sm-offset-4">
                    <button class="btn btn-sm btn-success" type="submit">
                        <i class="fa fa-search-plus" aria-hidden="true"></i> Search
                    </button>
                    <button class="btn btn-sm btn-default" id="btn_block_reset" type="reset">
                        <i class="fa fa-trash" aria-hidden="true"></i> Reset
                    </button>
                </div>
            </div>
        </div>
    </form>
    <div id="block_spin" class="overlay" style="display:none;">
        <i class="fa fa-spin fa-spinner"></i>
    </div>
    <!-- /.box-body -->
</div>