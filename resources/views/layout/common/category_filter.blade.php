<div class="box box-success">
    <div class="box-header with-border">
        <i aria-hidden="true" class="fa fa-search-plus">
        </i>
        <h3 class="box-title">
            Category Search
        </h3>
    </div>
    <form class="form-horizontal" id="frm_filter" name="frm_filter" role="form">
        <div class="box-body">
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    Category :-
                </label>
                <div class="col-sm-7" id="divlocalchildcat">
                    {{ Form::select('category',[],Input::old('category'),array('id' => 'main_category','class'=>'form-control select2 input-sm local_category_search')) }}
                </div>
            </div>
            <!-- @if(Request::segment(1) == "ebay")
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        Ebay Category :-
                    </label>
                    <div class="col-sm-7">
                        {{ Form::select('categories',[],Input::old('categories'),array('class'=>'form-control select2 ebay_cat_search')) }}
                    </div>
                </div>
            @elseif(Request::segment(1) == "amazon")
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        Amazon Category :-
                    </label>
                    <div class="col-sm-7">
                        {{ Form::select('categories',[],Input::old('categories'),array('class'=>'form-control select2 amazon_cat_search','id' => 'amz_category')) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        Amazon Product Type Category :-
                    </label>
                    <div class="col-sm-7">
                        <div id="amzprodchildcat">
                            {{ Form::select('amzprodcat',$amzProdCat,Input::old('amzprodcat'),array('class'=>'form-control amzprodparent')) }}
                        </div>
                    </div>
                </div>
            @elseif(Request::segment(1) == "google")
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        Google Category :-
                    </label>
                    <div class="col-sm-7">
                        {!! Form::select('categories',[],Input::old('categories'),array('class'=>'form-control select2 google_cat_search','id' => 'google_category')) !!}
                        <p class="help-inline text-danger" id="categories_error">
                        </p>
                    </div>
                </div>
            @endif -->
        </div>
        <div class="box-footer">
            <div class="col-md-offset-2">
                <button class="btn btn-sm btn-success" type="submit">
                    Search Category
                </button>
                <button class="btn btn-sm btn-default" id="btn_reset" type="reset">
                    Reset
                </button>
            </div>
        </div>
    </form>
    <div class="overlay" id="search_spin" style="display:none;">
        <i class="fa fa-spin fa-spinner">
        </i>
    </div>
</div>