@if(isset($type) && $type == "local")
<div class="form-group required">
    <label class="col-sm-2 control-label">
        Local Category :-
    </label>
    <div class="col-sm-7">
        <div id="localchildcat">
            {{ Form::select('parentcat',[],Input::old('parentcat'),array('class'=>'form-control select2 local_category_search')) }}
            <span class="help-inline text-danger" id="parentcat_error">
            </span>
        </div>
    </div>
</div>
@elseif(isset($type) && $type == "local_cat")
<div class="form-group required">
    <label class="col-sm-3 control-label">
        Local Category :-
    </label>
    <div class="col-sm-7">
        <div id="localchildcat">
            {{ Form::select('parentcat',[],Input::old('parentcat'),array('class'=>'form-control select2 local_category_search')) }}
            <span class="help-inline text-danger" id="parentcat_error">
            </span>
        </div>
    </div>
</div>
@elseif(isset($type) && $type == "ebay")
<div class="form-group required">
    <label class="col-sm-2 control-label">
        Ebay Category :-
    </label>
    <div class="col-sm-7">
        {{ Form::select('categories',[],Input::old('categories'),array('class'=>'form-control select2 ebay_cat_search')) }}
        <span class="help-inline text-danger" id="categories_error">
        </span>
    </div>
</div>
@elseif(isset($type) && $type == "amazonCat")
<div class="form-group">
    <label class="col-sm-3 control-label">
        Amazon Category :-
    </label>
    <div class="col-sm-7">
        {{ Form::select('categories',[],Input::old('categories'),array('class'=>'form-control select2 amazon_cat_search','id' => 'amz_category')) }}
        <span class="help-inline text-danger" id="categories_error">
        </span>
    </div>
</div>
@elseif(isset($type) && $type == "amazonProductCat")
<div class="form-group required">
    <label class="col-sm-3 control-label">
        Amazon Product Type Category :-
    </label>
    <div class="col-sm-7">
        <div id="amzprodchildcat">
            {{ Form::select('amzprodcat',$amzProdCat,Input::old('amzprodcat'),array('class'=>'form-control amzprodparent')) }}
            <span class="help-inline text-danger" id="amzprodcat_error">
        </span>
        </div>
    </div>
</div>
@elseif(isset($type) && $type == "force")
<div class="form-group">
    <label class="col-sm-3 control-label">
    </label>
    <div class="col-sm-8">
        <label>{{ Form::checkbox('force_product',1,null,['class' => 'force_product']) }} Force overwrite to all product</label>
    </div>
</div>
@elseif(isset($type) && $type == "force_two")
<div class="form-group">
    <label class="col-sm-2 control-label">
    </label>
    <div class="col-sm-8">
        <label>{{ Form::checkbox('force_product',1,null,['class' => 'force_product']) }} Force overwrite to all product</label>
    </div>
</div>
@elseif(isset($type) && $type == "force_modal")
<div class="form-group">
    <label class="col-sm-3 control-label">  
    </label>
    <div class="col-sm-8">
        <label>{{ Form::checkbox('force_product',1,null,['class' => 'force_product']) }}
        Force overwrite to all product</label>
    </div>
</div>
@elseif(isset($type) && $type == "google")
<div class="form-group required">
    <label class="col-sm-3 control-label">
        Google Category :-
    </label>
    <div class="col-sm-7">
        {!! Form::select('categories',[],Input::old('categories'),array('class'=>'form-control google_cat_search','id' => 'google_category')) !!}
        <p class="help-inline text-danger" id="categories_error">
        </p>
    </div>
</div>
@elseif(isset($type) && $type == "walmart")
<div class="form-group required">
    <label class="col-sm-3 control-label">
        Walmart Category :-
    </label>
    <div class="col-sm-7">
        {!! Form::select('categories',[],Input::old('categories'),array('class'=>'form-control walmart_cat_search','id' => 'walmart_category')) !!}
        <p class="help-inline text-danger" id="categories_error">
        </p>
    </div>
</div>
@elseif(isset($type) && $type == "amazon_notes")
<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">
            Note :-
        </h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="remove" type="button">
                <i class="fa fa-times">
                </i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <span class="text-green">
            <i>
                1. When you block or unblock a product, it's impact is uploaded to the marketplace with a special batch that is run every hour'. You do not need to remove / delete the product from the marketplace unless the urgency of the situation is such that you have to
            </i>
        </span>
    </div>
    <div class="overlay" id="arrange_spin" style="display:none;">
        <i class="fa fa-spin fa-spinner">
        </i>
    </div>
    <!-- /.box-body -->
</div>
@elseif(isset($type) && $type == "global_shipping_notes")
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">
            Note :-
        </h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="remove" type="button">
                <i class="fa fa-times">
                </i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <span class="text-green">
            <i>
                1. From Here using global setting user can specify the specific product will ship with eBay global shipping program
                <br>
                2. Make Sure if you set the eBay global shipping is Yes for speicific product than internation shipping calculation will not be apply. eBay will handle all the thinks 
            </i>
        </span>
    </div>
    <div class="overlay" id="spin" style="display:none;">
        <i class="fa fa-spin fa-spinner">
        </i>
    </div>
    <!-- /.box-body -->
</div>
@elseif(isset($type) && $type == "amazon_suggestion_notes")
<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">
            Note :-
        </h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="remove" type="button">
                <i class="fa fa-times">
                </i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <span class="text-green">
            <i>
                Amazon Suggestions Section is for products that do not have a UPC code or an ASIN yet. 
            </i>
        </span>
    </div>
</div>
@elseif(isset($type) && $type == "amazon_suggestion_map")
<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">
            Note :-
        </h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="remove" type="button">
                <i class="fa fa-times">
                </i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <span class="text-green">
            <i>
                These are products that did not have a UPC code but were mapped manually with ASIN. <br> 
                These products will stay in this section forever.
            </i>
        </span>
    </div>
</div>
@elseif(isset($type) && $type == "amazon_suggestion_unmap")
<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">
            Note :-
        </h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="remove" type="button">
                <i class="fa fa-times">
                </i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <span class="text-green">
            <i>
                These products are unmapped. <br> The Amazon Suggestions did not result in much help. Manual action required.
            </i>
        </span>
    </div>
</div>
@elseif(isset($type) && $type == "amazon_top_map_category_notes")
<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">
            Note :-
        </h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="remove" type="button">
                <i class="fa fa-times">
                </i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <span class="text-green">
            <i>
                You can map your Local categories to Amazon using the upper part of this page. This will allow you to map categories whether they have quantity or not.
            </i>
        </span>
    </div>
</div>
@elseif(isset($type) && $type == "amazon_bottam_map_category_notes")
<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">
            Note :-
        </h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="remove" type="button">
                <i class="fa fa-times">
                </i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <span class="text-green">
            <i>
                <p>This part presents local product categories that have product quantities greater than zero. It presents both mapped and unmapped categories. You can change mapping as you like. </p>

                <p>To look at Local Categories that don't have a mapped Amazon Category and Amazon Product Category, click on the column Amazon Product Category and that will sort and present unmapped categories. Click on the right most 'Map Category' icon and a mapping menu will pop up.</p>

                <p>In Amazon Product Type Category, mapping only the first 2 levels are good enough. </p>

                <p> If you are adding a new product that has a UPC code, when UMP sends it to Amazon, Amazon checks the UPC and if that UPC already has an associated ASIN, then Amazon assigns it the same ASIN and the same product category type. If it does not exist in the Amazon database and has no ASIN, then Amazon assigns it a new ASIN and uses the product type category information provided through UMP. </p>

                <p> If you first create a product in Amazon, then your categorization and description rules on Amazon. If somebody else adds the product first, they rule. You list your products under their categorization and description. </p>
            </i>
        </span>
    </div>
</div>
@elseif(isset($type) && $type == "ebay_top_map_category_notes")
<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">
            Note :-
        </h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="remove" type="button">
                <i class="fa fa-times">
                </i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <span class="text-green">
            <i>
                You can map your Local categories to eBay using the upper part of this page. This will allow you to map categories whether they have quantity or not.
            </i>
        </span>
    </div>
    <div class="overlay" id="block_spin" style="display:none;">
        <i class="fa fa-spin fa-spinner">
        </i>
    </div>
</div>
@elseif(isset($type) && $type == "ebay_bottam_map_category_notes")
<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">
            Note :-
        </h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="remove" type="button">
                <i class="fa fa-times">
                </i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <span class="text-green">
            <i>
                <p>This part presents local product categories that have product quantities greater than zero. It presents both mapped and unmapped categories. You can change mapping as you like. </p>

                <p>To look at Local Categories that don't have a mapped eBay Category , click on the column eBay Category and that will sort and present unmapped categories. Click on the right most 'Map Category' icon and a mapping menu will pop up.</p>
            </i>
        </span>
    </div>
    <div class="overlay" id="search_spin" style="display:none;">
        <i class="fa fa-spin fa-spinner">
        </i>
    </div>
</div>
@elseif(isset($type) && $type == "block")
<div class="pull-right box-tools">
    <a class="btn btn-success btn-sm" data-toggle="tooltip" href="{{ route('products.create') }}" id="add_product" title="Add product">
        <i class="fa fa-fw fa-plus">
        </i>
    </a>
    <a class="btn btn-danger btn-sm btn-delete" data-name="All" data-toggle="tooltip" data-type="product" href="javascript:void(0)" title="Delete product">
        <i class="fa fa-lg fa fa-trash-o">
        </i>
    </a>

    <a class="btn btn-danger btn-sm blockProduct" data-toggle="tooltip" data-value="1" data-where="id" href="javascript:void(0)" title="Blocked Product">
        <i class="fa fa-lg fa-ban">
        </i>
    </a>
    <a class="btn btn-primary btn-sm blockProduct" data-toggle="tooltip" data-value="0" data-where="id" href="javascript:void(0)" title="Unblock Product">
        <i class="fa fa-lg fa-check-square-o">
        </i>
    </a>

    @include("layout.common.select",['type' => 'block_modal'])
</div>
@elseif(isset($type) && $type == "timestamp")
<div class="pull-right box-tools">
    <span class="badge bg-green">
        <b>
            Last Updated At :
        </b>
        {!! date("m-d-Y H:i:s",strtotime($product->updated_at)) !!}
    </span>
</div>
@elseif(isset($type) && $type == 'price')
<div class="tab-content">
    <div class="tab-pane active">
        @include("layout.common.form_filter")
        <div class="globalPriceFilter">
        </div>
        <div class="box box-warning">
            <div class="box-header with-border">
                <i aria-hidden="true" class="fa fa-usd">
                </i>
                <h3 class="box-title">
                    Price Setting
                </h3>
            </div>
            <div class="box-body">
                <form class="form-inline" role="form" id="price_filter_form">
                    <div class="row">
                        <div class="col-sm-6">
                            @foreach(['one'=>'$10.00 or less','two' => '$10.01 to $25.00','three' => '$25.01 to $40.00','four' => '$40.01 to $50.00'] as $key => $value)
                                <div class="form-group price_filter_{{$key}}">
                                    <label class="control-label">
                                        {{$value}} :-
                                    </label>
                                    <div class="checkbox">
                                        <label>
                                            <input id="radio_percentage_{{$key}}" name="chose_type_price_{{$key}}" type="radio" value="percentage" class="priceFilter" data-key="{{$key}}">
                                                %age
                                            </input>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input id="radio_fixprice_{{$key}}" name="chose_type_price_{{$key}}" type="radio" value="fixprice" class="priceFilter" data-key="{{$key}}">
                                                Fixed Amount
                                            </input>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input id="radio_both_{{$key}}" name="chose_type_price_{{$key}}" type="radio" value="both" class="priceFilter" data-key="{{$key}}">
                                                %age + Fixed Amount
                                            </input>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group percentage">
                                    <input class="form-control numeric" id="price_increase_input_{{$key}}" name="price_increase_{{$key}}" placeholder="Enter Percentage" type="text"/>
                                    <span class="help-inline text-danger" id="price_increase_{{$key}}_error"></span>
                                </div>
                                <div class="form-group dollarVal dollarValue{{$key}} hide">
                                    <input class="form-control numeric" id="price_increase_value_{{$key}}" name="price_value_{{$key}}" placeholder="Enter Amount" type="text"/>
                                    <span class="help-inline text-danger" id="price_value_{{$key}}_error"></span>
                                </div>
                                <hr>
                            @endforeach
                        </div>
                        <div class="col-sm-6">
                            @foreach(['five'=>'$50.01 to $75.00','six' => '$75.01 to $100.00','seven' => 'Above $100.01'] as $key => $value)
                                <div class="form-group price_filter_{{$key}}">
                                    <label class="control-label">
                                        {{$value}} :-
                                    </label>
                                    <div class="checkbox">
                                        <label>
                                            <input id="radio_percentage_{{$key}}" name="chose_type_price_{{$key}}" type="radio" value="percentage" class="priceFilter" data-key="{{$key}}">
                                                %age
                                            </input>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input id="radio_fixprice_{{$key}}" name="chose_type_price_{{$key}}" type="radio" value="fixprice" class="priceFilter" data-key="{{$key}}">
                                                Fixed Amount
                                            </input>
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input id="radio_both_{{$key}}" name="chose_type_price_{{$key}}" type="radio" value="both" class="priceFilter" data-key="{{$key}}">
                                                %age + Fixed Amount
                                            </input>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group percentage">
                                    <input class="form-control numeric" id="price_increase_input_{{$key}}" name="price_increase_{{$key}}" placeholder="Enter Percentage" type="text"/>
                                    <span class="help-inline text-danger" id="price_increase_{{$key}}_error"></span>
                                </div>
                                <div class="form-group dollarVal dollarValue{{$key}} hide">
                                    <input class="form-control numeric" id="price_increase_value_{{$key}}" name="price_value_{{$key}}" placeholder="Enter Amount" type="text"/>
                                    <span class="help-inline text-danger" id="price_value_{{$key}}_error"></span>
                                </div>
                                <hr>
                            @endforeach
                        </div>
                    </div>
                    <div class="text-center">
                        <a class="btn btn-sm btn-primary popovers btn-price" data-content="Submit to this changing {{ucfirst($platform)}} price to all searched products and if search not apply then it apply to all products with changes based on original price" data-selection="true" data-class="increase_price" data-type="update" data-toggle="popover" href="javascript:void(0)" title="Change {{ucfirst($platform)}} Price by Search">
                            <i class="fa fa-search-plus" aria-hidden="true"></i> Apply to Search Products
                        </a>
                        <a class="btn btn-sm btn-info popovers btn-price" data-content="Submit to this changing {{ucfirst($platform)}} price to selected products with changes based on original price" data-class="increase_price" data-type="update" data-selection="false" data-toggle="popover" href="javascript:void(0)" title="Change {{ucfirst($platform)}} Price by Selection">
                            <i class="fa fa-check-square-o" aria-hidden="true"></i> Apply to Selected Products
                        </a>
                        <!-- <a class="btn btn-sm btn-success popovers btn-price" data-content="Submit to this changing {{ucfirst($platform)}} price to all products with changes based on original price" data-class="increase_price" data-type="update" data-selection="all" data-toggle="popover" href="javascript:void(0)" title="Change {{ucfirst($platform)}} Price of All Products">
                            <i class="fa fa-th-list" aria-hidden="true"></i> Apply to All Products
                        </a> -->
                        <a class="btn btn-sm btn-default popovers btn-price-reset" data-content="Submit to this changing {{ucfirst($platform)}} price reset to original price for Search products" data-selection="true" data-class="reset_price" data-type="reset" data-toggle="popover" href="javascript:void(0)" title="Reset {{ucfirst($platform)}} Price by Search">
                            <i class="fa fa-eraser" aria-hidden="true"></i> Reset Search Products Price
                        </a>
                        <a class="btn btn-sm btn-default popovers btn-price-reset" data-content="Submit to this changing {{ucfirst($platform)}} price reset to original price for Selected products" data-selection="false" data-class="reset_price" data-type="reset" data-toggle="popover" href="javascript:void(0)" title="Reset {{ucfirst($platform)}} Selected Product Price">
                            <i class="fa fa-eraser" aria-hidden="true"></i> Reset Selected Products Price
                        </a>
                        <!-- <a class="btn btn-sm btn-danger popovers btn-price-reset" data-content="Submit to this changing {{ucfirst($platform)}} price to original price for all products" data-class="reset_price" data-selection="all" data-type="reset" data-toggle="popover" href="javascript:void(0)" title="Reset All {{ucfirst($platform)}} Product Price">
                            <i class="fa fa-eraser" aria-hidden="true"></i> Reset All Price
                        </a> -->
                    </div>
                </form>
            </div>
            <div class="overlay" id="price_arran_spin" style="display:none;">
                <i class="fa fa-spin fa-spinner">
                </i>
            </div>
        </div>
        <div class="globalShippingFilter">
        </div>
        <div class="box box-info">
            <div class="box-header with-border">
                <i aria-hidden="true" class="fa fa-usd">
                </i>
                <h3 class="box-title">
                    Shipping Markup
                </h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" role="form" id="shipping_filter_form">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Less than 1.00 lbs:-</label>
                                <div class="col-sm-6">
                                    <input class="form-control numeric" id="txt_first" name="txt_first" placeholder="Enter value" type="text"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-5 control-label">1.01 to 2.00 lbs:-</label>
                                <div class="col-sm-6">
                                    <input class="form-control numeric" id="txt_second" name="txt_second" placeholder="Enter value" type="text"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-5 control-label">2.01 to 4.00 lbs:-</label>
                                <div class="col-sm-6">
                                    <input class="form-control numeric" id="txt_third" name="txt_third" placeholder="Enter value" type="text"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-5 control-label">4.01 to 7.00 lbs:-</label>
                                <div class="col-sm-6">
                                    <input class="form-control numeric" id="txt_four" name="txt_four" placeholder="Enter value" type="text"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">7.01 to 10.00 lbs:-</label>
                                <div class="col-sm-6">
                                    <input class="form-control numeric" id="txt_five" name="txt_five" placeholder="Enter value" type="text"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-5 control-label">10.01 to 15.00 lbs:-</label>
                                <div class="col-sm-6">
                                    <input class="form-control numeric" id="txt_six" name="txt_six" placeholder="Enter value" type="text"/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-5 control-label">15.01 to 20.00 lbs:-</label>
                                <div class="col-sm-6">
                                    <input class="form-control numeric" id="txt_seven" name="txt_seven" placeholder="Enter value" type="text"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-5 control-label">20.01 to 25.00 lbs:-</label>
                                <div class="col-sm-6">
                                    <input class="form-control numeric" id="txt_eight" name="txt_eight" placeholder="Enter value" type="text"/>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">25.01 to 30.00 lbs :-</label>
                                <div class="col-sm-6">
                                    <input class="form-control numeric" id="txt_nighn" name="txt_nighn" placeholder="Enter value" type="text"/>
                                </div>
                            </div>
                            
                             <div class="form-group">
                                <label class="col-sm-5 control-label">30.01 to 35.00 lbs:-</label>
                                <div class="col-sm-6">
                                    <input class="form-control numeric" id="txt_ten" name="txt_ten" placeholder="Enter value" type="text"/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-5 control-label">35.01 to 50.00 lbs :-</label>
                                <div class="col-sm-6">
                                    <input class="form-control numeric" id="txt_eleven" name="txt_eleven" placeholder="Enter value" type="text"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-5 control-label">50.01 to 70.00 lbs :-</label>
                                <div class="col-sm-6">
                                    <input class="form-control numeric" id="txt_twelve" name="txt_twelve" placeholder="Enter value" type="text"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="col-sm-6 control-label"> 70.01 lbs and above :-</label>
                                <div class="col-sm-6">
                                    <input class="form-control numeric" id="txt_thirteen" name="txt_thirteen" placeholder="Enter value" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <a class="btn btn-sm btn-primary popovers btn-shipping" data-content="Submit to this changing {{ucfirst($platform)}} price to all searched products and if search not apply then it apply to all products with changes based on original price" data-selection="true" data-class="increase_price" data-type="update" data-toggle="popover" href="javascript:void(0)" title="Change {{ucfirst($platform)}} Price by Search">
                            <i class="fa fa-search-plus" aria-hidden="true"></i> Apply to Search Products
                        </a>
                        <a class="btn btn-sm btn-info popovers btn-shipping" data-content="Submit to this changing {{ucfirst($platform)}} price to selected products with changes based on original price" data-class="increase_price" data-type="update" data-selection="false" data-toggle="popover" href="javascript:void(0)" title="Change {{ucfirst($platform)}} Price by Selection">
                            <i class="fa fa-check-square-o" aria-hidden="true"></i> Apply to Selected Products
                        </a>
                        <!-- <a class="btn btn-sm btn-success popovers btn-shipping" data-content="Submit to this changing {{ucfirst($platform)}} price to all products with changes based on original price" data-class="increase_price" data-type="update" data-selection="all" data-toggle="popover" href="javascript:void(0)" title="Change {{ucfirst($platform)}} Price of All Products">
                            <i class="fa fa-th-list" aria-hidden="true"></i> Apply to All Products
                        </a> -->
                        <a class="btn btn-sm btn-default popovers btn-shipping-reset" data-content="Submit to this changing {{ucfirst($platform)}} price reset to original price for Search products" data-selection="true" data-class="reset_price_shipping" data-type="reset" data-toggle="popover" href="javascript:void(0)" title="Reset {{ucfirst($platform)}} Shipping Price by Search">
                            <i class="fa fa-eraser" aria-hidden="true"></i> Reset Search Products Shipping Price
                        </a>
                        <a class="btn btn-sm btn-default popovers btn-shipping-reset" data-content="Submit to this changing {{ucfirst($platform)}} price reset to original price for Selected products" data-selection="false" data-class="reset_price_shipping" data-type="reset" data-toggle="popover" href="javascript:void(0)" title="Reset {{ucfirst($platform)}} Selected Product Shipping Price">
                            <i class="fa fa-eraser" aria-hidden="true"></i> Reset Selected Products Shipping Price
                        </a>
                        <!-- <a class="btn btn-sm btn-danger popovers btn-shipping-reset" data-content="Submit to this changing {{ucfirst($platform)}} price to original price for all products" data-class="reset_price_shipping" data-selection="all" data-type="reset" data-toggle="popover" href="javascript:void(0)" title="Reset All {{ucfirst($platform)}} Product Shipping Price">
                            <i class="fa fa-eraser" aria-hidden="true"></i> Reset All Price
                        </a> -->
                    </div>
                </form>
            </div>
            <div class="overlay" id="shipping_arran_spin" style="display:none;">
                <i class="fa fa-spin fa-spinner">
                </i>
            </div>
        </div>
        <div class="box box-info">
            <div class="box-header with-border">
                <i aria-hidden="true" class="fa fa-snowflake-o">
                </i>
                <h3 class="box-title">
                    Notes
                </h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="remove" type="button">
                        <i class="fa fa-times">
                        </i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <span class="text-green">
                    <i>
                        Price setting markup is applied to Selling Price, Not Cost Price.
                    </i>
                </span>
                <span class="text-green">
                    <i>
                        Cost is not included in calculation,it is for reference only,Final selling price is the sum of selling price + price markup + shipping markup
                    </i>
                </span>
            </div>
            <div class="overlay" id="arrange_spin" style="display:none;">
                <i class="fa fa-spin fa-spinner">
                </i>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="box box-primary">
            <div class="box-header with-border">
                <i aria-hidden="true" class="fa fa-product-hunt">
                </i>
                <h3 class="box-title">
                    Products list
                </h3>
                <div class="pull-right box-tools">
                </div>
                <!-- /. tools -->
            </div>
            <div class="box-body">
                <table class="table table-condensed table-bordered table-hover" id="productPrice" style="width:100%">
                    <thead>
                        <tr>
                            <th style="padding-left:20px;">
                                <input id="selectall" type="checkbox"/>
                            </th>
                            <th>
                                Image
                            </th>
                            <th>
                                Part No./SKU
                            </th>
                            <th>
                                Title
                            </th>
                            <th>
                                Distributor
                            </th>
                            <th>
                                Distributor SKU
                            </th>
                            <th>
                                Weight
                            </th>
                            <th>
                                Cost
                            </th>
                            <th>
                                Selling Price
                            </th>
                            <th>
                                Price Markup
                            </th>
                            <th>
                                Shipping Markup
                            </th>
                            <th>
                                {{ucfirst($platform)}} Selling Price
                            </th>
                        </tr>
                    </thead>
                    <thead id="filters">
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="overlay" id="spin" style="display:none;">
                <i class="fa fa-spin fa-spinner">
                </i>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>
@section('script')
<script type="text/javascript">

$(function() {
    var productPrice = $('#productPrice').dataTable({
        "bProcessing": false,
        "bServerSide": true,
        "autoWidth": true,
        "bFilter": false,
        "bStateSave": true,
        "sAjaxSource": public_path + platform + "/price",
        "sPaginationType": "listbox",
        "oLanguage": {
          "sLengthMenu": "Display _MENU_ records"
        },
        "fnServerParams": function ( aoData ) {
            var form_data = $('#frm_filter').serializeArray();

            $.each(form_data, function(i, val){
                aoData.push(val);
            });
            aoData.push({ "name": "act", "value": "fetch" });
            server_params = aoData;
        },
        "aaSorting": [
            [2, "asc"]
        ],
        "aoColumns": [{
            mData: "id",
            bSortable: false,
            bVisible: true,
            sWidth: "5%",
            sClass: 'text-center no_click_action',
            mRender: function(v, t, o) {
                return '<input type="checkbox" class="cls_product_id" id="chk_' + v + '" name="product_id[]" value="' + v + '"/>';
            },
        },
        {
            "mData":"image",
            bSortable:false,
            sWidth: "5%",
            sClass: 'text-center',
            mRender : function(v, t, o){
                if (v != null && v != '') {
                    act_html = "<img src='"+v+"' height='35' width='35' class='product-thumbnail'/>";
                    return act_html;
                };
                return '<i class="fa fa-picture-o"></i>';
            }
        },
        { "mData": "product_id",sWidth: "6%" },
        { "mData": "title",sWidth: "25%" },
        { "mData": "vendor",sWidth: "5%" },
        { "mData": "vendor_sku",sWidth: "10%" },
        { "mData": "weight",sWidth: "5%" },
        { "mData": "unit_cost",sWidth: "8%" },
        {
    "mData":"unit_price",
    bSortable:false,
    sWidth: "5%",
    mRender : function(v, t, o){
        return v.toFixed(2);
    }
},
        {
            mData:platform + "_price_filter_value",
            bSortable:true,
            sClass : 'no_click_action',
            sWidth: "8%",
            mRender : function(v,t,o){
                return parseFloat(v).toFixed(2);
            }
        },
        { "mData": platform + "_shipping_price",sWidth: "10%" },
        { "mData": platform + "_price",sWidth: "12%",sClass: 'cursorPointer no_click_action' },
        ],
        fnPreDrawCallback: function() {
            myShow();
        },
        fnDrawCallback: function(oSettings) {
            myHide();
            $('#productPrice tbody td:last-child').editable( public_path + platform + '/price/edit', {
                cancel: '<button class="btn btn-xs btn-flat btn-danger" type="cancel" >Cancel</button>',
                submit: '<button class="btn btn-xs btn-flat btn-success" type="submit" >Ok</button>',
                tooltip: 'Click to edit...',
                indicator: 'Saving...',
                height: "25px",
                style: "cursor: pointer;",
                submitdata: function () {
                    var ele = $(this).closest('tr').get(0);
                    data = product.fnGetData( ele );
                    return data;
                },
                callback: function( value, settings ) {
                    var resp = $.parseJSON(value);
                    if (resp.success == false) {
                        swal({
                            title: "There Were Some Error Tryagain",
                            type: "warning",
                            timer: 3000,
                            showConfirmButton: true
                        });
                    } else {
                        swal({
                        title: "Product Price Updated successfully",
                            type: "success",
                            timer: 3000,
                            showConfirmButton: true
                        });
                    };
                    var redrawtable = $('#productPrice').dataTable();
                    redrawtable.fnStandingRedraw();

                },
            });

            $("table td").livequery('dblclick', function() {
                var isProductEdit = $(this).is('.no_click_action');
                if( ! isProductEdit ) {
                    var ele = $(this).closest('tr').get(0);
                    var id = productPrice.fnGetData( ele )['id'];
                    var url = "{{ route('products.show',':id') }}";
                    url = url.replace(':id',id);
                    window.open(url, '_blank');
                }
            });
        }
    });
});
</script>
@stop
@include("layout.common.select",['type' => 'block_modal'])
@elseif(isset($type) && $type == 'quantity')
    <div class="tab-content">
        @include("layout.common.form_filter")
        <div class="globalQuantityFilter">
        </div>
        <div class="box box-primary">
            <div class="box-header with-border">
                <i aria-hidden="true" class="fa fa-usd">
                </i>
                <h3 class="box-title">
                    Quantity Setting
                </h3>
            </div>
            <div class="box-body">
                <form class="form-inline" role="form">
                    <div class="form-group">
                        <label class="control-label">
                            Change By:
                        </label>
                        <div class="checkbox">
                            <label>
                                <input checked="true" id="ebay_quantity" name="chose_type_quantity" type="radio" value="{{$platform}}_quantity">
                                    Quantity
                                </input>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input id="min_{{$platform}}_quantity" name="chose_type_quantity" type="radio" value="min_{{$platform}}_quantity">
                                    Minimum Quantity
                                </input>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input class="form-control" id="quantity_increase_input" name="quantity_increase" placeholder="Enter Quantity" type="text"/>
                    </div>
                    
                    <a class="btn btn-sm btn-primary popovers btn-quantity" data-content="Submit to this changing {{ucfirst($platform)}} quantity to all searched products and if search not apply then it apply to all products with changes based on original quantity" data-selection="true" data-class="increase_quantity" data-type="update" data-toggle="popover" href="javascript:void(0)" title="Change {{ucfirst($platform)}} Quantity by Search">
                        <i class="fa fa-search-plus" aria-hidden="true"></i> Apply to Search Products
                    </a>
                    <a class="btn btn-sm btn-info popovers btn-quantity" data-content="Submit to this changing {{ucfirst($platform)}} quantity to selected products with changes based on original quantity" data-class="increase_price" data-type="update" data-selection="false" data-toggle="popover" href="javascript:void(0)" title="Change {{ucfirst($platform)}} Quantity by Selection">
                        <i class="fa fa-check-square-o" aria-hidden="true"></i> Apply to Selected Products
                    </a>
                    <!-- <a class="btn btn-sm btn-success popovers btn-quantity" data-content="Submit to this changing {{ucfirst($platform)}} quantity to all products with changes based on original quantity" data-class="increase_price" data-type="update" data-selection="all" data-toggle="popover" href="javascript:void(0)" title="Change {{ucfirst($platform)}} Quantity of All Products">
                        <i class="fa fa-th-list" aria-hidden="true"></i> Apply to All Products
                    </a> -->

                    <a class="btn btn-sm btn-default popovers btn-quantity-reset" data-content="Submit to this changing {{ucfirst($platform)}} quantity to original quantity for Search products" data-selection="true" data-class="reset_quantity" data-type="reset" data-toggle="popover" href="javascript:void(0)" title="Reset {{ucfirst($platform)}} Quantity by Search">
                        <i class="fa fa-eraser" aria-hidden="true"></i> Reset Search Quantity
                    </a>
                    <a class="btn btn-sm btn-default popovers btn-quantity-reset" data-content="Submit to this changing {{ucfirst($platform)}} quantity to original quantity for selected products" data-selection="false" data-class="reset_quantity" data-type="reset" data-toggle="popover" href="javascript:void(0)" title="Reset {{ucfirst($platform)}} Selected Product Quantity">
                        <i class="fa fa-eraser" aria-hidden="true"></i> Reset Selected Quantity 
                    </a>
                    <!-- <a class="btn btn-sm btn-danger popovers btn-quantity-reset" data-content="Submit to this changing {{ucfirst($platform)}} quantity to original quantity for all products" data-class="reset_quantity" data-selection="all" data-type="reset" data-toggle="popover" href="javascript:void(0)" title="Reset All {{ucfirst($platform)}} Product Quantity">
                        <i class="fa fa-eraser" aria-hidden="true"></i> Reset All Quantity
                    </a> -->
                </form>
                <Br>
                <span class="text-green">
                    <i>
                        Notes:- If Quantity is less than minimum quantity than product will be automatically set out of stock for {{ucfirst($platform)}} 
                    </i>
                </span>
            </div>
            <div class="overlay" id="quantity_spin" style="display:none;">
                <i class="fa fa-spin fa-spinner">
                </i>
            </div>
        </div>
        <div class="box box-info">
            <div class="box-header with-border">
                <i aria-hidden="true" class="fa fa-product-hunt">
                </i>
                <h3 class="box-title">
                    Products list
                </h3>
                <div class="pull-right box-tools">
                </div>
                <!-- /. tools -->
            </div>
            <div class="box-body">
                <table class="table table-condensed table-bordered table-hover" id="product" style="width:100%">
                    <thead>
                        <tr>
                            <th style="padding-left:20px;">
                                <input id="selectall" type="checkbox"/>
                            </th>
                            <th>
                                Image
                            </th>
                            <th>
                                Part No./SKU
                            </th>
                            <th>
                                Title
                            </th>
                            <th>
                                Quantity
                            </th>
                            <th>
                                {{ucfirst($platform)}} Quantity
                            </th>
                            <th>
                                Min {{ucfirst($platform)}} Quantity
                            </th>
                        </tr>
                    </thead>
                    <thead id="filters">
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="overlay" id="spin" style="display:none;">
                <i class="fa fa-spin fa-spinner">
                </i>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
@include("layout.common.select",['type' => 'block_modal'])
@elseif(isset($type) && $type == 'global_block') 
    <div class="tab-content">
        <div class="tab-pane active">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <i aria-hidden="true" class="fa fa-snowflake-o">
                    </i>
                    <h3 class="box-title">
                        Notes
                    </h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="remove" type="button">
                            <i class="fa fa-times">
                            </i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <span class="text-green" style="font-size: 15px">
                        <i>
                            <ul>
                                <li> Here you can define whether a distributors products are automatically blocked or not. </li>
                                <li>If you select blocked, then every time new products from this distributor are uploaded to UMP, they will be blocked.</li>
                                <li> You will get an email that new products have been uploaded and are blocked. </li>
                                <li> You can manually review them and unblock them if you like. </li>
                            </ul>
                        </i>
                    </span>
                </div>
                <div class="overlay" id="price_spin" style="display:none;">
                    <i class="fa fa-spin fa-spinner">
                    </i>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i aria-hidden="true" class="fa fa-product-hunt">
                    </i>
                    <h3 class="box-title">
                        Distributor list
                    </h3>
                    @include("layout.common.select",['type' => 'block_button'])
                    <!-- /. tools -->
                </div>
                <div class="box-body">
                    <table class="table table-condensed table-bordered table-hover" id="globalBlock" style="width:100%">
                        <thead>
                            <tr>
                                <th style="padding-left:20px;">
                                    <input id="selectall" type="checkbox"/>
                                </th>
                                <th>
                                    Distributor Name
                                </th>
                                <th>
                                    Product Count
                                </th>
                                <th>
                                    New Product Blocked Status
                                </th>
                                <th>
                                    Notes
                                </th>
                                <th>
                                    {{ucfirst($platform)}} Blocked Status
                                </th>
                            </tr>
                        </thead>
                        <thead id="filters">
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="overlay" id="spin" style="display:none;">
                    <i class="fa fa-spin fa-spinner">
                    </i>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    @section('script')
        <script type="text/javascript">
            $(function() {    
                var globalBlock = $('#globalBlock').dataTable({
                    "bProcessing": false,
                    "bServerSide": true,
                    "autoWidth": true,
                    "bStateSave": true,
                    "bDestroy": true,
                    "sAjaxSource": '{{URL::route("$platform.global-block.get")}}',
                    "sPaginationType": "listbox",
                    "oLanguage": {
                      "sLengthMenu": "Display _MENU_ records"
                    },
                    "bFilter": false,
                    "fnServerParams": function ( aoData ) {
                        //send other data to server side
                        var form_data = $('#frm_filter').serializeArray();

                        $.each(form_data, function(i, val){
                            aoData.push(val);
                        });
                        aoData.push({ "name": "act", "value": "fetch" });
                        server_params = aoData;
                    },
                    "aaSorting": [
                        [2, "desc"]
                    ],
                    "aoColumns": [{
                        mData: "name",
                        bSortable: false,
                        bVisible: true,
                        sWidth: "3%",
                        sClass: 'text-center no_click_action',
                        mRender: function(v, t, o) {
                            return '<input type="checkbox" id="chk_' + v + '" name="product_id[]" value="' + v + '"/>';
                        },
                    },
                    { "mData": "name",sWidth: "25%"},
                    {
                        "mData": "product_count",
                        sWidth: "10%",
                        sClass: "text-center",
                        mRender: function(v, t, o) {
                            if (v == null || v == '') {
                                v = 0;
                            }
                            var url = '{{ URL::route("$platform.blockproduct.get",["vendor",":id"])}}';
                            url = url.replace(':id',o['name']);
                            var act_html = "<a id="+v+" target='_blank' href='"+url+"' class='btn' data-toggle='tooltip' title='View Category Product'><span class='badge bg-blue'>"+v+"</span></a>";
                            return act_html;
                        }
                    },
                    {
                        mData:'force_product_new',
                        bSortable:false,
                        sClass : 'text-center no_click_action',
                        sWidth: "15%",
                        mRender : function(v,t,o){
                            if (v == 0) {
                                var act_html = "<span class='badge bg-green'>No</span></a>";
                            } else {
                                var act_html = "<span class='badge bg-red'>Yes</span></a>";
                            }
                            return act_html;
                        }
                    },
                    {
                        mData:null,
                        bSortable:true,
                        sClass : 'text-center no_click_action',
                        sWidth: "7%",
                        mRender : function(v,t,o){
                            var act_html = "<a id="+v+" href='javascript:void(0);' data-type='Blocked Notes' data-where='global' data-blockId='"+o['name']+"' class='btn btn-warning btn-xs blockProductNotes' data-toggle='tooltip' title='Blocked Product Notes'><i class='fa fa-lg fa-pencil'></i></a>";
                            return act_html;
                        }
                    },
                    {
                        mData:"{{$platform}}_blocked_status",
                        bSortable:false,
                        sClass : 'text-center no_click_action',
                        sWidth: "10%",
                        mRender : function(v,t,o){
                            if (v == 0) {
                                var act_html = "<a id="+v+" href='javascript:void(0);' data-type='block' data-toggle='tooltip' data-where='global' data-value='1' data-blockId='"+o['name']+"' class='btn btn-success btn-xs blockProduct' title='UnBlocked Product'><i class='fa fa-lg fa-check-square-o'></i></a>";
                            } else {
                                var act_html = "<a id="+v+" href='javascript:void(0);' data-type='Unblock' data-where='global' data-blockId='"+o['name']+"' data-value='0' class='btn btn-danger btn-xs blockProduct' data-toggle='tooltip' title='Blocked Product'><i class='fa fa-lg fa-ban'></i></a>";
                            }
                            return act_html;
                        }
                    },
                    ],
                    fnPreDrawCallback: function() {
                        myShow();
                    },
                    fnDrawCallback: function(oSettings) {
                        myHide();
                    }
                });

                $("#globalBlock thead#filters input:text").donetyping(function() {
                    globalBlock.fnFilter(this.value, $("#globalBlock thead#filters input:text").index(this));
                });

                $('#change_category').on('change',function() {
                    globalBlock.fnFilter(this.value, $("#globalBlock thead#filters input:text").index(this));
                });
            });
        </script>
    @stop
@elseif(isset($type) && $type == 'block_modal') 
    <!-- Meta Confirm -->
    <div class="modal" data-backdrop="static" id="confirmMeta">
        <div class="modal-dialog">
            <div class="modal-content box">
                <div class="modal-header">
                    <button aria-hidden="true" class="close" data-dismiss="modal" type="button">
                        ×
                    </button>
                    <h4 class="modal-title" style="text-transform: capitalize;">
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="callout callout-success" id="modal_map_success" style="display:none;">
                        <p id="success">   
                            This is success   
                        </p>
                    </div>
                    <p></p>
                    <br>
                    <div class="row">
                        <div class="forceRow hide">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-0 control-label">
                                    </label>
                                    <div class="col-sm-12">
                                        <label>
                                        {{ Form::checkbox('force_product',1,null,['class' => 'force_modal_product']) }} 
                                        Force overwrite to all product
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="forceNewRow hide">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-0 control-label">
                                    </label>
                                    <div class="col-sm-12">
                                        <label>
                                        {{ Form::checkbox('force_product_new',1,null,['class' => 'force_modal_product_new']) }} 
                                        Force overwrite to new product
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="blockNotes">
                    </div>
                    <br>
                    <div class="block_Notes" style="display: none">
                        <span class="text-red">
                            <B><i> Notes :- </i> </B>
                            <ol>
                                <li>
                                    <i>
                                        Be care fully while block any product from here, It will  permenently block those for all marketplace and shopping engine.
                                    </i>
                                </li>
                                <li>
                                    <i>
                                        If any product is uploaded to the specific marketplace or shopping engine then product is also remove from that.
                                    </i>
                                </li> 
                            </ol>
                        </span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal" type="button">
                        <i class="fa fa-times-circle-o" aria-hidden="true"></i> Cancel
                    </button>
                    <button class="btn model_click" id="confirm" type="submit" style="text-transform: capitalize;">
                        <i class="fa fa-th-list" aria-hidden="true"></i> Update
                    </button>
                </div>
                <div class="overlay" id="modal_spin" style="display:none;">
                    <i class="fa fa-spin fa-spinner">
                    </i>
                </div>
            </div>
        </div>
    </div>
    <!-- Meta Confirm -->
@elseif(isset($type) && $type == 'block_button')     
    <div class="pull-right">
        <a class="btn btn-danger btn-sm blockProduct" data-type="block" data-where="{{$where}}" data-value="1" data-toggle="tooltip" href="javascript:void(0)" title="Blocked Product" data-keyword="{!! isset($keyword) ? $keyword:'null' !!}">
            <i class="fa fa-lg fa-ban">
            </i>
        </a>
        <a class="btn btn-success btn-sm blockProduct" data-type="Unblock" data-value="0" data-where="{{$where}}" data-toggle="tooltip" href="javascript:void(0)" title="Unblocked Product" data-keyword="{!! isset($keyword) ? $keyword :'null' !!}">
            <i class="fa fa-lg fa-check-square-o">
            </i>
        </a>
    </div>
    @include("layout.common.select",['type' => 'block_modal'])
@elseif(isset($type) && $type == 'product')
    <div class="callout callout-success" id="product-delete-success" style="display:none">
        <p>
            product successfully Deleted.
        </p>
    </div>
    <div class="box box-{{$cls}}">
        <div class="box-header with-border">
            <i aria-hidden="true" class="fa fa-product-hunt">
            </i>
            <h3 class="box-title">
                {{$title}}
            </h3>
            @include("layout.common.select",['type' => 'block'])
            <!-- /. tools -->
        </div>
        <div class="box-body">
            <div class="nav-tabs-custom tabs-custom1">
                @if($active == 'block')
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                Note :-
                            </h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="remove" type="button">
                                    <i class="fa fa-times">
                                    </i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body">
                            <span class="text-red">
                                <i>
                                    If you block or unblock a product here, it will be blocked or unblocked in all marketplaces and shopping engines
                                </i>
                            </span>
                        </div>
                        <div class="overlay" id="arrange_spin" style="display:none;">
                            <i class="fa fa-spin fa-spinner">
                            </i>
                        </div>
                        <!-- /.box-body -->
                    </div>
                @endif
                <ul class="nav nav-tabs">
                    <li role="presentation" class="{{$active=='all'?'active':''}}">
                        <a href="{!! route('products.index') !!}">
                            All Products
                        </a>
                    </li>

                    <li role="presentation" class="{{$active=='ready'?'active':''}}">
                        <a href="{!! route('products.ready') !!}">
                            Ready Products
                        </a>
                    </li>
                    <li role="presentation" class="{{$active=='quantity'?'active':''}}">
                        <a href="{!! route('products.zeroquantity') !!}">
                            Quantity = 0
                        </a>
                    </li>
                    <li role="presentation" class="{{$active=='price'?'active':''}}">
                        <a href="{!! route('products.zeroprice') !!}">
                            Price = 0
                        </a>
                    </li>
                    <li role="presentation" class="{{$active=='block'?'active':''}}">
                        <a href="{!! route('products.blocked') !!}">
                            Blocked Products
                        </a>
                    </li>
                    <li role="presentation" class="{{$active=='primary_image'?'active':''}}">
                        <a href="{!! route('products.withoutPrimaryImage') !!}">
                            Products without Primary images
                        </a>
                    </li>
                    <li role="presentation" class="{{$active=='second_image'?'active':''}}">
                        <a href="{!! route('products.withoutimage') !!}">
                            Products without Secondary images
                        </a>
                    </li>
                    <li role="presentation" class="{{$active=='zero_weight'?'active':''}}">
                        <a href="{!! route('products.zeroweight') !!}">
                            Quantity > 0 & Weight = 0
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active">
                        <table class="table table-condensed table-bordered table-hover" id="product" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="padding-left:20px;">
                                        <input id="selectall" type="checkbox"/>
                                    </th>
                                    <th>
                                        Image
                                    </th>
                                    <th>
                                        Part No
                                    </th>
                                    <th>
                                        Vendor SKU
                                    </th>
                                    <th>
                                        Title
                                    </th>
                                    <th>
                                        Quantity
                                    </th>
                                    @if($userId=='1' && $active == 'all')
                                    <th>
                                        Dallas Quantity
                                    </th>
                                    @endif
                                    <th>
                                        Price
                                    </th>
                                    <th>
                                        Blocked Status
                                    </th>
                                    @if($active == 'block')
                                        <th>
                                            Blocked Notes
                                        </th>
                                    @endif
                                    <th>
                                        Parent/Child
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <thead id="filters">
                                <th>
                                </th>
                                <th>
                                </th>
                                <th>
                                    <input class="form-control" style="width:80px" placeholder="SKU" />
                                </th>
                                <th>
                                    <input class="form-control" style="width:80px" placeholder="Vendor SKU" />
                                </th>
                                <th>
                                    <input class="form-control" placeholder="Title"/>
                                    <input class="form-control" placeholder="UPC Code"/>
                                </th>
                                <th>
                                </th>
                                <th>
                                </th>
                                <th>
                                </th>
                                <th>
                                </th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="overlay" id="spin" style="display:none;">
            <i class="fa fa-spin fa-spinner">
            </i>
        </div>
        <!-- /.box-body -->
    </div>
@elseif(isset($type) && $type == 'block_notes') 
    <div class="tab-content">
        <div class="tab-pane active">
            <div class="box box-info">
                <div class="box-header with-border">
                    <i class="fa fa-deaf"></i>
                    <h3 class="box-title">
                        Add Notes
                    </h3>
                </div>
                {!! Form::open(array('url'=>route("$platform.notes.post"),'class' => 'form-horizontal')) !!}
                <div class="box-body">
                    <div class="row">
                        <div class="form-group required">
                            <label class="col-sm-2 control-label">Notes:-</label>
                            <div class="col-sm-6">
                                {!! Form::textarea('note',Input::get('note',$note),array('class'=>'form-control','placeholder'=>'Block Notes')) !!}
                                {!! $errors->first('note','<span class="help-inline text-danger">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="form-group">
                        <div class="col-sm-offset-4">
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-sliders" aria-hidden="true"></i> Save
                            </button>
                            <button type="reset" class="btn btn-default">
                                <i class="fa fa-trash" aria-hidden="true"></i> Reset
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
                <div id="spin" class="overlay" style="display:none;">
                    <i class="fa fa-spin fa-spinner"></i>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@section('script')

<script type="text/javascript">

$(document).ready(function(){
  $('form').submit(function(){
    myShow();
  });

  CKEDITOR.config.allowedContent = true;
    CKEDITOR.config.extraAllowedContent = '*(*);*{*}';
    CKEDITOR.replace('note', {
        toolbar: [
            ["Source"],
            ["Bold","Italic","Underline"],
            ["Link","Unlink","Anchor"],
            ["Styles","Format","Font","FontSize"],
            ["TextColor","BGColor"],
            ["UIColor","Maximize","ShowBlocks"],
            "/",
            ["NumberedList","BulletedList","Outdent","Indent","Blockquote","CreateDiv","JustifyLeft","JustifyCenter","JustifyRight","JustifyBlock"],
        ]
    });
});

</script>
@stop
@elseif(isset($type) && $type == 'manufacturer_block') 
    <!-- Tab panes -->
    <div class="tab-content">
        @if(isset($isNotes) && $isNotes == 'Yes')
            @include("layout.common.select",['type' => 'amazon_notes'])
        @endif
        <div class="tab-pane active">
            @include("layout.common.manufacturer_filter")
            <div class="box box-info">
                <div class="box-header with-border">
                    <i aria-hidden="true" class="fa fa-product-hunt">
                    </i>
                    <h3 class="box-title">
                        Manufacturer List
                    </h3>
                   @include("layout.common.select",['type' => 'block_button','where' => 'manufacturer'])
                    <!-- /. tools -->
                </div>
                <div class="box-body">
                    <table class="table table-condensed table-bordered table-hover" id="manufacturerBlock" style="width:100%">
                        <thead>
                            <tr>
                                <th style="padding-left:20px;">
                                    <input id="selectall" type="checkbox"/>
                                </th>
                                <th>
                                    Manufacrurer Name
                                </th>
                                <th>
                                    Product In Manufacrurer
                                </th>
                                <th>
                                    New Product Blocked Status
                                </th>
                                <th> Notes </th>
                                <th>
                                    {{ucfirst($platform)}} Blocked Status
                                </th>
                            </tr>
                        </thead>
                        <thead id="filters">
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="overlay" id="spin" style="display:none;">
                    <i class="fa fa-spin fa-spinner">
                    </i>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    @section('script')
        <script type="text/javascript">
            $(function() {    
                var manufacturerBlock = $('#manufacturerBlock').dataTable({
                    "bProcessing": false,
                    "bServerSide": true,
                    "autoWidth": true,
                    "bStateSave": true,
                    "bDestroy": true,
                    "sAjaxSource": '{{URL::route("$platform.blockmanufacturer.get",$where)}}',
                    "sPaginationType": "listbox",
                    "oLanguage": {
                      "sLengthMenu": "Display _MENU_ records"
                    },
                    "bFilter": false,
                    "fnServerParams": function ( aoData ) {
                        //send other data to server side
                        var form_data = $('#frm_filter').serializeArray();

                        $.each(form_data, function(i, val){
                            aoData.push(val);
                        });
                        aoData.push({ "name": "act", "value": "fetch" });
                        server_params = aoData;
                    },
                    "aaSorting": [
                        [2, "desc"]
                    ],
                    "aoColumns": [
                    {
                        mData: "id",
                        bSortable: false,
                        bVisible: true,
                        sWidth: "5%",
                        sClass: 'text-center no_click_action',
                        mRender: function(v, t, o) {
                            return '<input type="checkbox" id="chk_' + v + '" name="manufacturer[]" value="' + o['name'] + '"/>';
                        },
                    },
                    { "mData": "name",sWidth: "25%",},
                    {
                        "mData": "product_count",
                        bSortable: true,
                        sWidth: "15%",
                        sClass: "text-center",
                        mRender: function(v, t, o) {
                            if (v == null || v == '') {
                                v = 0;
                            }
                            var url = '{{ URL::route("$platform.blockproduct.get",['manufacturer',':id'])}}';
                            url = url.replace(':id',o['name']);
                            var act_html = "<a id="+v+" target='_blank' href='"+url+"' class='btn' data-toggle='tooltip' title='View Category Product'><span class='badge bg-blue'>"+v+"</span></a>";
                            return act_html;
                        }
                    },
                    {
                        mData:'force_product_new',
                        bSortable:false,
                        sClass : 'text-center no_click_action',
                        sWidth: "15%",
                        mRender : function(v,t,o){
                            if (v == 0) {
                                var act_html = "<span class='badge bg-green'>No</span></a>";
                            } else {
                                var act_html = "<span class='badge bg-red'>Yes</span></a>";
                            }
                            return act_html;
                        }
                    },
                    {
                        mData:null,
                        bSortable:true,
                        sClass : 'text-center no_click_action',
                        sWidth: "7%",
                        mRender : function(v,t,o){
                            var act_html = "<a id="+v+" href='javascript:void(0);' data-type='Blocked Notes' data-where='manufacturer' data-blockId='"+o['name']+"' class='btn btn-warning btn-xs blockProductNotes' data-toggle='tooltip' title='Blocked Product Notes'><i class='fa fa-lg fa-pencil'></i></a>";
                            return act_html;
                        }
                    },
                    {
                        mData:"{{$platform}}_blocked_status",
                        bSortable:false,
                        sClass : 'text-center no_click_action',
                        sWidth: "10%",
                        mRender : function(v,t,o){
                            if (v == 0) {
                                var act_html = "<a id="+v+" href='javascript:void(0);' data-type='block' data-toggle='tooltip' data-where='manufacturer' data-value='1' data-blockId='"+o['name']+"' class='btn btn-success btn-xs blockProduct' title='UnBlocked Product'><i class='fa fa-lg fa-check-square-o'></i></a>";
                            } else {
                                var act_html = "<a id="+v+" href='javascript:void(0);' data-type='Unblock' data-where='manufacturer' data-blockId='"+o['name']+"' data-value='0' class='btn btn-danger btn-xs blockProduct' data-toggle='tooltip' title='Blocked Product'><i class='fa fa-lg fa-ban'></i></a>";
                            }
                            return act_html;
                        }
                    },
                    ],
                    fnPreDrawCallback: function() {
                        myShow();
                    },
                    fnDrawCallback: function(oSettings) {
                        myHide();

                        $("table td").livequery('dblclick', function() {
                            var isProductEdit = $(this).is('.no_click_action');
                            if( ! isProductEdit ) {
                                var ele = $(this).closest('tr').get(0);
                                var id = product.fnGetData( ele )['id'];
                                var url = "{{ route('products.show',':id') }}";
                                url = url.replace(':id',id);
                                window.open(url, '_blank');
                            }
                        });
                    }
                });

                $("#manufacturerBlock thead#filters input:text").donetyping(function() {
                    manufacturerBlock.fnFilter(this.value, $("#manufacturerBlock thead#filters input:text").index(this));
                });

                $('#manufacturer').on('change',function() {
                    manufacturerBlock.fnFilter(this.value, $("#manufacturerBlock thead#filters input:text").index(this));
                });
            });
        </script>
    @stop
@elseif(isset($type) && $type == 'category_block') 
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active">
            @include("layout.common.category_filter")
            <div class="box box-info">
                <div class="box-header with-border">
                    <i aria-hidden="true" class="fa fa-product-hunt">
                    </i>
                    <h3 class="box-title">
                        Category list
                    </h3>
                    @include("layout.common.select",['type' => 'block_button','where' => 'category'])
                    <!-- /. tools -->
                </div>
                <div class="box-body">
                    <table class="table table-condensed table-bordered table-hover" id="categoryBlock" style="width:100%">
                        <thead>
                            <tr>
                                <th style="padding-left:20px;">
                                        <input id="selectall" type="checkbox"/>
                                    </th>
                                <th>
                                    Category Name
                                </th>
                                @if($platform !='shopping')
                                    <th>
                                        {{ucfirst($platform)}} Category Name
                                    </th>
                                @endif
                                @if($platform =='amazon')
                                    <th>
                                        {{ucfirst($platform)}} Product Category Name
                                    </th>
                                @endif
                                <th>
                                    Product In Category
                                </th>
                                <th>
                                    New Product Blocked Status
                                </th>
                                <th> Notes </th>
                                <th>
                                    {{ucfirst($platform)}} Blocked Status
                                </th>
                            </tr>
                        </thead>
                        <thead id="filters">
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="overlay" id="spin" style="display:none;">
                    <i class="fa fa-spin fa-spinner">
                    </i>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    @section('script')
        <script type="text/javascript">
            $(function() { 
                if(platform == 'shopping') {
                    var categoryBlock = $('#categoryBlock').dataTable({
                        "bProcessing": false,
                        "bServerSide": true,
                        "autoWidth": true,
                        "bStateSave": true,
                        "sAjaxSource": '{{URL::route("$platform.blockcategory.get",$where)}}',
                        "sPaginationType": "listbox",
                        "oLanguage": {
                          "sLengthMenu": "Display _MENU_ records"
                        },
                        "bFilter": false,
                        "fnServerParams": function ( aoData ) {
                            //send other data to server side
                            var form_data = $('#frm_filter').serializeArray();

                            $.each(form_data, function(i, val){
                                aoData.push(val);
                            });
                            aoData.push({ "name": "act", "value": "fetch" });
                            server_params = aoData;
                        },
                        "aaSorting": [
                            [1, "desc"]
                        ],
                        "aoColumns": [{
                            mData: "id",
                            bSortable: false,
                            bVisible: true,
                            sWidth: "5%",
                            sClass: 'text-center',
                            mRender: function(v, t, o) {
                                return '<input type="checkbox" id="chk_' + v + '" name="category_id[]" value="' + v + '"/>';
                            },
                        },
                        {
                            "mData":"local_category",
                            sWidth: "30%",
                            mRender : function(v, t, o){
                                if (v == null || v == '') {
                                    return '';
                                };
                                catTree = v.replaceAll('>','<i class="fa fa-fw fa-lg fa-angle-double-right"></i>')
                                act_html = "<div class='local_cat_div' data-value='"+ o['id'] +"'>"+catTree+"</div>"
                                return act_html;
                            }
                        },
                        {
                            "mData": "product_count",
                            bSortable: true,
                            sWidth: "10%",
                            sClass: "text-center",
                            mRender: function(v, t, o) {
                                if (v == null || v == '') {
                                    v = 0;
                                }
                                var url = '{{ URL::route("ebay.blockproduct.get",["category",":id"])}}';
                                url = url.replace(':id',o['id']);
                                var act_html = "<a id="+v+" target='_blank' href='"+url+"' class='btn' data-toggle='tooltip' title='View Category Product'><span class='badge bg-blue'>"+v+"</span></a>";
                                return act_html;
                            }
                        },
                        {
                            mData:'force_product_new',
                            bSortable:false,
                            sClass : 'text-center no_click_action',
                            sWidth: "15%",
                            mRender : function(v,t,o){
                                if (v == 0) {
                                    var act_html = "<span class='badge bg-green'>No</span></a>";
                                } else {
                                    var act_html = "<span class='badge bg-red'>Yes</span></a>";
                                }
                                return act_html;
                            }
                        },
                        {
                            mData:null,
                            bSortable:true,
                            sClass : 'text-center no_click_action',
                            sWidth: "7%",
                            mRender : function(v,t,o){
                                var act_html = "<a id="+v+" href='javascript:void(0);' data-type='Blocked Notes' data-where='category_id' data-blockId='"+o['id']+"' class='btn btn-warning btn-xs blockProductNotes' data-toggle='tooltip' title='Blocked Product Notes'><i class='fa fa-lg fa-pencil'></i></a>";
                                return act_html;
                            }
                        },
                        {
                            mData:"{{$platform}}_blocked_status",
                            bSortable:false,
                            sClass : 'text-center no_click_action',
                            sWidth: "10%",
                            mRender : function(v,t,o){
                                if (v == 0) {
                                    var act_html = "<a id="+v+" href='javascript:void(0);' data-type='block' data-toggle='tooltip' data-where='category' data-value='1' data-blockId='"+o['id']+"' class='btn btn-success btn-xs blockProduct' title='UnBlocked Product'><i class='fa fa-lg fa-check-square-o'></i></a>";
                                } else {
                                    var act_html = "<a id="+v+" href='javascript:void(0);' data-type='Unblock' data-where='category' data-blockId='"+o['id']+"' data-value='0' class='btn btn-danger btn-xs blockProduct' data-toggle='tooltip' title='Blocked Product'><i class='fa fa-lg fa-ban'></i></a>";
                                }
                                return act_html;
                            }
                        },
                        ],
                        fnPreDrawCallback: function() {
                            myShow();
                        },
                        fnDrawCallback: function(oSettings) {
                            myHide();
                        }
                    });
                }  else if(platform == 'amazon') {
                    var categoryBlock = $('#categoryBlock').dataTable({
                        "bProcessing": false,
                        "bServerSide": true,
                        "autoWidth": true,
                        "bStateSave": true,
                        "sAjaxSource": '{{URL::route("$platform.blockcategory.get",$where)}}',
                        "sPaginationType": "listbox",
                        "oLanguage": {
                          "sLengthMenu": "Display _MENU_ records"
                        },
                        "bFilter": false,
                        "fnServerParams": function ( aoData ) {
                            //send other data to server side
                            var form_data = $('#frm_filter').serializeArray();

                            $.each(form_data, function(i, val){
                                aoData.push(val);
                            });
                            aoData.push({ "name": "act", "value": "fetch" });
                            server_params = aoData;
                        },
                        "aaSorting": [
                            [1, "desc"]
                        ],
                        "aoColumns": [{
                            mData: "id",
                            bSortable: false,
                            bVisible: true,
                            sWidth: "5%",
                            sClass: 'text-center',
                            mRender: function(v, t, o) {
                                return '<input type="checkbox" id="chk_' + v + '" name="category_id[]" value="' + v + '"/>';
                            },
                        },
                        {
                            "mData":"local_category",
                            sWidth: "30%",
                            mRender : function(v, t, o){
                                if (v == null || v == '') {
                                    return '';
                                };
                                catTree = v.replaceAll('>','<i class="fa fa-fw fa-lg fa-angle-double-right"></i>')
                                act_html = "<div class='local_cat_div' data-value='"+ o['id'] +"'>"+catTree+"</div>"
                                return act_html;
                            }
                        },
                        {
                            "mData":"{{$platform}}_category",
                            sWidth: "20%",
                            mRender : function(v, t, o){
                                if (v == null || v == '') {
                                    return '';
                                };
                                catTree = v.replaceAll(' >> ','<i class="fa fa-fw fa-lg fa-angle-double-right"></i>')
                                act_html = "<div class='local_cat_div' data-value='"+ o['id'] +"'>"+catTree+"</div>"
                                return act_html;
                            }
                        },
                        {
                            "mData":"{{$platform}}_product_category",
                            sWidth: "20%",
                            mRender : function(v, t, o){
                                if (v == null || v == '') {
                                    return '';
                                };
                                catTree = v.replaceAll(' >> ','<i class="fa fa-fw fa-lg fa-angle-double-right"></i>')
                                act_html = "<div class='local_cat_div' data-value='"+ o['id'] +"'>"+catTree+"</div>"
                                return act_html;
                            }
                        },
                        {
                            "mData": "product_count",
                            bSortable: true,
                            sWidth: "10%",
                            sClass: "text-center",
                            mRender: function(v, t, o) {
                                if (v == null || v == '') {
                                    v = 0;
                                }
                                var url = '{{ URL::route("ebay.blockproduct.get",["category",":id"])}}';
                                url = url.replace(':id',o['id']);
                                var act_html = "<a id="+v+" target='_blank' href='"+url+"' class='btn' data-toggle='tooltip' title='View Category Product'><span class='badge bg-blue'>"+v+"</span></a>";
                                return act_html;
                            }
                        },
                        {
                            mData:'force_product_new',
                            bSortable:false,
                            sClass : 'text-center no_click_action',
                            sWidth: "15%",
                            mRender : function(v,t,o){
                                if (v == 0) {
                                    var act_html = "<span class='badge bg-green'>No</span></a>";
                                } else {
                                    var act_html = "<span class='badge bg-red'>Yes</span></a>";
                                }
                                return act_html;
                            }
                        },
                        {
                            mData:null,
                            bSortable:true,
                            sClass : 'text-center no_click_action',
                            sWidth: "7%",
                            mRender : function(v,t,o){
                                var act_html = "<a id="+v+" href='javascript:void(0);' data-type='Blocked Notes' data-where='id' data-blockId='"+o['id']+"' class='btn btn-warning btn-xs blockProductNotes' data-toggle='tooltip' title='Blocked Product Notes'><i class='fa fa-lg fa-pencil'></i></a>";
                                return act_html;
                            }
                        },
                        {
                            mData:"{{$platform}}_blocked_status",
                            bSortable:false,
                            sClass : 'text-center no_click_action',
                            sWidth: "10%",
                            mRender : function(v,t,o){
                                if (v == 0) {
                                    var act_html = "<a id="+v+" href='javascript:void(0);' data-type='block' data-toggle='tooltip' data-where='category' data-value='1' data-blockId='"+o['id']+"' class='btn btn-success btn-xs blockProduct' title='UnBlocked Product'><i class='fa fa-lg fa-check-square-o'></i></a>";
                                } else {
                                    var act_html = "<a id="+v+" href='javascript:void(0);' data-type='Unblock' data-where='category' data-blockId='"+o['id']+"' data-value='0' class='btn btn-danger btn-xs blockProduct' data-toggle='tooltip' title='Blocked Product'><i class='fa fa-lg fa-ban'></i></a>";
                                }
                                return act_html;
                            }
                        },
                        ],
                        fnPreDrawCallback: function() {
                            myShow();
                        },
                        fnDrawCallback: function(oSettings) {
                            myHide();
                        }
                    });
                } else {
                    var categoryBlock = $('#categoryBlock').dataTable({
                        "bProcessing": false,
                        "bServerSide": true,
                        "autoWidth": true,
                        "bStateSave": true,
                        "sAjaxSource": '{{URL::route("$platform.blockcategory.get",$where)}}',
                        "sPaginationType": "listbox",
                        "oLanguage": {
                          "sLengthMenu": "Display _MENU_ records"
                        },
                        "bFilter": false,
                        "fnServerParams": function ( aoData ) {
                            //send other data to server side
                            var form_data = $('#frm_filter').serializeArray();

                            $.each(form_data, function(i, val){
                                aoData.push(val);
                            });
                            aoData.push({ "name": "act", "value": "fetch" });
                            server_params = aoData;
                        },
                        "aaSorting": [
                            [1, "desc"]
                        ],
                        "aoColumns": [{
                            mData: "id",
                            bSortable: false,
                            bVisible: true,
                            sWidth: "5%",
                            sClass: 'text-center',
                            mRender: function(v, t, o) {
                                return '<input type="checkbox" id="chk_' + v + '" name="category_id[]" value="' + v + '"/>';
                            },
                        },
                        {
                            "mData":"local_category",
                            sWidth: "30%",
                            mRender : function(v, t, o){
                                if (v == null || v == '') {
                                    return '';
                                };
                                catTree = v.replaceAll('>','<i class="fa fa-fw fa-lg fa-angle-double-right"></i>')
                                act_html = "<div class='local_cat_div' data-value='"+ o['id'] +"'>"+catTree+"</div>"
                                return act_html;
                            }
                        },
                        {
                            "mData":"{{$platform}}_category",
                            sWidth: "30%",
                            mRender : function(v, t, o){
                                if (v == null || v == '') {
                                    return '';
                                };
                                catTree = v.replaceAll(' >> ','<i class="fa fa-fw fa-lg fa-angle-double-right"></i>')
                                act_html = "<div class='local_cat_div' data-value='"+ o['id'] +"'>"+catTree+"</div>"
                                return act_html;
                            }
                        },
                        {
                            "mData": "product_count",
                            bSortable: true,
                            sWidth: "10%",
                            sClass: "text-center",
                            mRender: function(v, t, o) {
                                if (v == null || v == '') {
                                    v = 0;
                                }
                                var url = '{{ URL::route("ebay.blockproduct.get",["category",":id"])}}';
                                url = url.replace(':id',o['id']);
                                var act_html = "<a id="+v+" target='_blank' href='"+url+"' class='btn' data-toggle='tooltip' title='View Category Product'><span class='badge bg-blue'>"+v+"</span></a>";
                                return act_html;
                            }
                        },
                        {
                            mData:'force_product_new',
                            bSortable:false,
                            sClass : 'text-center no_click_action',
                            sWidth: "15%",
                            mRender : function(v,t,o){
                                if (v == 0) {
                                    var act_html = "<span class='badge bg-green'>No</span></a>";
                                } else {
                                    var act_html = "<span class='badge bg-red'>Yes</span></a>";
                                }
                                return act_html;
                            }
                        },
                        {
                            mData:null,
                            bSortable:true,
                            sClass : 'text-center no_click_action',
                            sWidth: "7%",
                            mRender : function(v,t,o){
                                var act_html = "<a id="+v+" href='javascript:void(0);' data-type='Blocked Notes' data-where='id' data-blockId='"+o['id']+"' class='btn btn-warning btn-xs blockProductNotes' data-toggle='tooltip' title='Blocked Product Notes'><i class='fa fa-lg fa-pencil'></i></a>";
                                return act_html;
                            }
                        },
                        {
                            mData:"{{$platform}}_blocked_status",
                            bSortable:false,
                            sClass : 'text-center no_click_action',
                            sWidth: "10%",
                            mRender : function(v,t,o){
                                if (v == 0) {
                                    var act_html = "<a id="+v+" href='javascript:void(0);' data-type='block' data-toggle='tooltip' data-where='category' data-value='1' data-blockId='"+o['id']+"' class='btn btn-success btn-xs blockProduct' title='UnBlocked Product'><i class='fa fa-lg fa-check-square-o'></i></a>";
                                } else {
                                    var act_html = "<a id="+v+" href='javascript:void(0);' data-type='Unblock' data-where='category' data-blockId='"+o['id']+"' data-value='0' class='btn btn-danger btn-xs blockProduct' data-toggle='tooltip' title='Blocked Product'><i class='fa fa-lg fa-ban'></i></a>";
                                }
                                return act_html;
                            }
                        },
                        ],
                        fnPreDrawCallback: function() {
                            myShow();
                        },
                        fnDrawCallback: function(oSettings) {
                            myHide();
                        }
                    });
                }



                $("#categoryBlock thead#filters input:text").donetyping(function() {
                    categoryBlock.fnFilter(this.value, $("#categoryBlock thead#filters input:text").index(this));
                });

                $('#change_category').on('change',function() {
                    categoryBlock.fnFilter(this.value, $("#categoryBlock thead#filters input:text").index(this));
                });
            });
        </script>
    @stop
@elseif(isset($type) && $type == 'global_handling') 
    <div class="tab-content">
        <div class="tab-pane active">
            @include("ebay.handlingtime",['where' => 'vendor'])
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i aria-hidden="true" class="fa fa-product-hunt">
                    </i>
                    <h3 class="box-title">
                        Distributor list
                    </h3>
                </div>
                <div class="box-body">
                    <table class="table table-condensed table-bordered table-hover" id="globalHandling" style="width:100%">
                        <thead>
                            <tr>
                                <th style="padding-left:20px;">
                                    <input id="selectall" type="checkbox"/>
                                </th>
                                <th>
                                    Distributor Name
                                </th>
                                <th>
                                    Product Count
                                </th>
                                <th>
                                    {{ucfirst($platform)}} Shipping Time
                                </th>
                                <th>
                                    New Product Shipping Time
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <thead id="filters">
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="overlay" id="spin" style="display:none;">
                    <i class="fa fa-spin fa-spinner">
                    </i>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    @section('script')
        <script type="text/javascript">
            $(function() {    
                var globalHandling = $('#globalHandling').dataTable({
                    "bProcessing": false,
                    "bServerSide": true,
                    "autoWidth": true,
                    "bStateSave": true,
                    "bDestroy": true,
                    "sAjaxSource": '{{URL::route("$platform.global-handletime.get")}}',
                    "sPaginationType": "listbox",
                    "oLanguage": {
                      "sLengthMenu": "Display _MENU_ records"
                    },
                    "bFilter": false,
                    "fnServerParams": function ( aoData ) {
                        //send other data to server side
                        var form_data = $('#frm_filter').serializeArray();

                        $.each(form_data, function(i, val){
                            aoData.push(val);
                        });
                        aoData.push({ "name": "act", "value": "fetch" });
                        server_params = aoData;
                    },
                    "aaSorting": [
                        [2, "desc"]
                    ],
                    "aoColumns": [{
                        mData: "name",
                        bSortable: false,
                        bVisible: true,
                        sWidth: "3%",
                        sClass: 'text-center no_click_action',
                        mRender: function(v, t, o) {
                            return '<input type="checkbox" id="chk_' + v + '" name="product_id[]" value="' + v + '"/>';
                        },
                    },
                    { "mData": "name",sWidth: "25%"},
                    {
                        "mData": "product_count",
                        sWidth: "10%",
                        sClass: "text-center",
                        mRender: function(v, t, o) {
                            if (v == null || v == '') {
                                v = 0;
                            }
                            var url = '{{ URL::route("$platform.handletime-product.get",["vendor",":id"])}}';
                            url = url.replace(':id',o['name']);
                            var act_html = "<a id="+v+" target='_blank' href='"+url+"' class='btn' data-toggle='tooltip' title='View Category Product'><span class='badge bg-blue'>"+v+"</span></a>";
                            return act_html;
                        }
                    },
                    {
                        mData:"{{$platform}}_shipping_handling_time",
                        bSortable:false,
                        sClass : 'text-center no_click_action',
                        sWidth: "15%",
                        mRender : function(v,t,o){
                            return "<span class='badge bg-green'>"+v+" days</span></a>";
                        }
                    },
                    {
                        mData:'force_product_new',
                        bSortable:false,
                        sClass : 'text-center no_click_action',
                        sWidth: "15%",
                        mRender : function(v,t,o){
                            if (v == 0) {
                                var act_html = "<span class='badge bg-red'>No</span></a>";
                            } else {
                                var act_html = "<span class='badge bg-green'>Yes</span></a>";
                            }
                            return act_html;
                        }
                    },
                    {
                        mData: null,
                        bSortable:false,
                        sWidth: "5%",
                        sClass: 'text-center',
                        mRender : function(v, t, o){
                            var data = JSON.stringify(o).replaceAll('\'','');
                            act_html = "<div class='btn-group'>"
                                        +"<button data-toggle='tooltip' data-toggle='tooltip' data-where='vendor' data-value='"+o['name']+"' data-view='Vendor' data-name='"+ o['name'] +"' title='Handling Time' data-placement='top' class='btn btn-xs btn-success handlingTime'><i class='fa fa-exchange'></i></button>"
                                        +"</div>";
                            return act_html;
                        }
                    },
                    ],
                    fnPreDrawCallback: function() {
                        myShow();
                    },
                    fnDrawCallback: function(oSettings) {
                        myHide();
                    }
                });

                $("#globalHandling thead#filters input:text").donetyping(function() {
                    globalHandling.fnFilter(this.value, $("#globalHandling thead#filters input:text").index(this));
                });

                $('#change_category').on('change',function() {
                    globalHandling.fnFilter(this.value, $("#globalHandling thead#filters input:text").index(this));
                });
            });
        </script>
    @stop
@endif
