@if(isset($priceSetting) && !empty($priceSetting) && $type == 'price')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i aria-hidden="true" class="fa fa-usd">
            </i>
            <h3 class="box-title">
            Global Price Setting
            </h3>
            <div class="pull-right box-tools">
                <span class="badge bg-green">
                    <b>
                    Last Updated At :
                    </b>
                    {{ date('m-d-Y H:i:s',strtotime($priceSetting['updated_at']))}} (US Time)
                </span>
            </div>
        </div>
        <div class="box-body">
            <form class="form-horizontal" role="form" id="price_filter_show">
                <div class="row">
                    <div class="col-sm-6">
                        @foreach(['one'=>'$10 or less','two' => '$10.1 to $25','three' => '$25.1 to $40','four' => '$40.1 to $50'] as $key => $value)
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{{$value}} :-</label>
                            <div class="col-sm-10">
                                @if(isset($priceSetting[$key]))
                                <b> {{ucfirst($platform)}} Price Change By </b> :- <span class='badge bg-blue'> {{$priceSetting[$key]['price_status']}} </span>
                                <br>
                                <b> {{ucfirst($platform)}} Price Change Value </b> :- <span class='badge bg-blue'> {{$priceSetting[$key]['price_value']}} </span>
                                @else
                                -
                                @endif
                            </div>
                        </div>
                        <hr>
                        @endforeach
                    </div>
                    <div class="col-sm-6">
                        @foreach(['five'=>'$50.1 to $75','six' => '$75.1 to $100','seven' => '$100 and above'] as $key => $value)
                        <div class="form-group">
                            <label class="col-sm-3 control-label">{{$value}} :-</label>
                            <div class="col-sm-9">
                                @if(isset($priceSetting[$key]))
                                <b> {{ucfirst($platform)}} Price Change By </b> :- <span class='badge bg-blue'> {{$priceSetting[$key]['price_status']}} </span>
                                <br>
                                <b> {{ucfirst($platform)}} Price Change Value </b> :- <span class='badge bg-blue'> {{$priceSetting[$key]['price_value']}} </span>
                                @else
                                -
                                @endif
                            </div>
                        </div>
                        <hr>
                        @endforeach
                    </div>
                </div>
            </form>
        </div>
        <div class="overlay" id="price_spin" style="display:none;">
            <i class="fa fa-spin fa-spinner">
            </i>
        </div>
    </div>
@endif
@if(isset($shippingSetting) && !empty($shippingSetting) && $type == 'price')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i aria-hidden="true" class="fa fa-usd">
            </i>
            <h3 class="box-title">
            Global Shipping Markup
            </h3>
            <div class="pull-right box-tools">
                <span class="badge bg-green">
                    <b>
                    Last Updated At :
                    </b>
                    {{ date('m-d-Y H:i:s',strtotime($shippingSetting['updated_at']))}} (US Time)
                </span>
            </div>
        </div>
        <div class="box-body">
            <form class="form-horizontal" role="form" id="shipping_filter_show">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="col-sm-5 control-label">Less than 1.00 lbs:-</label>
                            <div class="col-sm-3">
                                <span class='badge bg-blue'>{{$shippingSetting['0.01 to 1.00']}}</span>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-5 control-label">1.01 to 2.00 lbs:-</label>
                            <div class="col-sm-3">
                                <span class='badge bg-blue'>{{$shippingSetting['1.01 to 2.00 lbs']}}</span>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-5 control-label">2.01 to 4.00 lbs:-</label>
                            <div class="col-sm-3">
                                <span class='badge bg-blue'>{{$shippingSetting['2.01 to 4.00 lbs']}}</span>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-5 control-label">4.01 to 7.00 lbs:-</label>
                            <div class="col-sm-3">
                                <span class='badge bg-blue'>{{$shippingSetting['4.01 to 7.00 lbs']}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="col-sm-5 control-label">7.01 to 10.00 lbs:-</label>
                            <div class="col-sm-3">
                                <span class='badge bg-blue'>{{$shippingSetting['7.01 to 10.00 lbs']}}</span>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-5 control-label">10.01 to 15.00 lbs:-</label>
                            <div class="col-sm-3">
                                <span class='badge bg-blue'>{{$shippingSetting['10.01 to 15.00 lbs']}}</span>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-5 control-label">15.01 to 20.00 lbs:-</label>
                            <div class="col-sm-3">
                                <span class='badge bg-blue'>{{$shippingSetting['15.01 to 20.00 lbs']}}</span>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-5 control-label">20.01 to 25.00 lbs:-</label>
                            <div class="col-sm-3">
                                <span class='badge bg-blue'>{{$shippingSetting['20.01 to 25.00 lbs']}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="col-sm-5 control-label">25.01 to 30.00 lbs :-</label>
                            <div class="col-sm-3">
                                <span class='badge bg-blue'>{{$shippingSetting['25.01 to 30.00 lbs']}}</span>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-5 control-label">30.01 to 35.00 lbs:-</label>
                            <div class="col-sm-3">
                                <span class='badge bg-blue'>{{$shippingSetting['30.01 to 35.00 lbs']}}</span>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-5 control-label">35.01 to 50.00 lbs :-</label>
                            <div class="col-sm-3">
                                <span class='badge bg-blue'>{{$shippingSetting['35.01 to 50.00 lbs']}}</span>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-5 control-label">50.01 to 70.00 lbs :-</label>
                            <div class="col-sm-3">
                                <span class='badge bg-blue'>{{$shippingSetting['50.01 to 70.00 lbs']}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="col-sm-5 control-label">70.01 lbs above :-</label>
                            <div class="col-sm-3">
                                <span class='badge bg-blue'>{{$shippingSetting['70.01 lbs above']}}</span>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
            </form>
        </div>
        <div class="overlay" id="shipping_spin" style="display:none;">
            <i class="fa fa-spin fa-spinner">
            </i>
        </div>
    </div>
@endif
@if(isset($quantitySetting) && !empty($quantitySetting) && $type =='quantity')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i aria-hidden="true" class="fa fa-usd">
            </i>
            <h3 class="box-title">
            Global Quantity Setting
            </h3>
            <div class="pull-right box-tools">
                <span class="badge bg-green">
                    <b>
                    Last Updated At :
                    </b>
                    {{ date('m-d-Y H:i:s',strtotime($quantitySetting['updated_at']))}} (US Time)
                </span>
            </div>
        </div>
        <div class="box-body">
            <form class="form-inline" role="form">
                <div class="form-group col-sm-4">
                    <label class="control-label">
                        {{ucfirst($platform)}} Min Quantity Change By :
                    </label>
                    <div class="form-group">
                        {{$quantitySetting['quantity_status']}}
                    </div>
                </div>
                <div class="form-group col-sm-4">
                    <label class="control-label">
                        {{ucfirst($platform)}} Min Quantity Change Value :
                    </label>
                    <div class="form-group">
                        {{$quantitySetting['quantity_value']}}
                    </div>
                </div>
                <div class="form-group col-sm-4">
                    <label class="control-label">
                        {{ucfirst($platform)}} Min Quantity Change Type :
                    </label>
                    <div class="form-group">
                        {{$quantitySetting['status']}}
                    </div>
                </div>
                <br>
                @if($quantitySetting['quantity_value'] > 0 && $quantitySetting['status'] == 'All')
                <div class="form-group col-sm-12">
                    <div class="form-group">
                        <span class="text-green">
                            <i>
                            Note:- If any new {{ucfirst($platform)}} product will be uploaded, it's {{ucfirst($platform)}} min quantity will be {{$quantitySetting['quantity_value']}}
                            </i>
                        </span>
                    </div>
                </div>
                @endif
                </br>
            </form>
        </div>
        <div class="overlay" id="price_spin" style="display:none;">
            <i class="fa fa-spin fa-spinner">
            </i>
        </div>
    </div>
@endif
@if(isset($blockPriceSetting) && !empty($blockPriceSetting) && $type =='block')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i aria-hidden="true" class="fa fa-usd">
            </i>
            <h3 class="box-title">
                Global Block Price Setting
            </h3>

            <div class="pull-right box-tools">
                <a href='javascript:void(0);' data-vendor="{{$vendorId}}" data-type='Unblock Filter' class='btn btn-danger removeFilter' data-toggle='tooltip' title='Remove Block Filter'><i class='fa fa-lg fa-trash'></i></a>
                @include("layout.common.select",['type' => 'block_modal'])
            </div>
        </div>
        <div class="box-body">
            <table id='blockFilter' class="table table-condensed table-bordered table-hover" style="width:100%">
                <thead>
                    <tr>
                        <th class="text-center">
                            <input id="selectall" type="checkbox"/>
                        </th>
                        <th class="text-center">Price Filter Name</th>
                        <th class="text-center">New Product Blocked Status</th>
                        <th class="text-center">Product Count</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($blockPriceSetting))
                        @foreach($blockPriceSetting as $key => $value)
                            <tr>
                                <td width="10%" class="text-center">
                                    <input type="checkbox" id='chk_{{$key}}' name="product_id[]" value="{{$value['id']}}"/>
                                </td>
                                <td class="text-center">
                                    {{$platform}} price {{ $value['price_filter']." ".$value['price_value'] }}
                                </td>
                                <td class="text-center">
                                    @if($value['force_product_new'] == 1)
                                        <span class="badge bg-green">Yes</span>
                                    @else
                                        <span class="badge bg-red">No</span>
                                    @endif
                                </td>
                                <td class="text-center">
                                    <span class="badge bg-green">{{$value['no_of_product']}}</span>
                                </td>
                                <td class="text-center">
                                    <a href='javascript:void(0);' data-type='Unblock Filter' data-where='id' data-blockId="{{$value['id']}}" data-vendor="{{$vendorId}}" class='btn btn-danger btn-xs removeFilter' data-toggle='tooltip' title='Blocked Product'><i class='fa fa-lg fa-trash'></i></a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        <div class="overlay" id="price_spin" style="display:none;">
            <i class="fa fa-spin fa-spinner">
            </i>
        </div>
    </div>
    @section('script')
        <script type="text/javascript">
            $(function() {
                blockFilter = $('#blockFilter').dataTable();
            });
        </script>
    @stop
@endif

@if(isset($handlingSetting) && !empty($handlingSetting))
    <div class="box box-primary">
        <div class="box-header with-border">
            <i aria-hidden="true" class="fa fa-usd">
            </i>
            <h3 class="box-title">
                Global Shipping Handling Setting
            </h3>

            <div class="pull-right box-tools">
                <a href='javascript:void(0);' data-vendor="{{$vendorId}}" class='btn btn-danger removeHandlingFilter' data-toggle='tooltip' title='Remove Filter'><i class='fa fa-lg fa-trash'></i></a>
                @include("layout.common.select",['type' => 'block_modal'])
            </div>
        </div>
        <div class="box-body">
            <table id='handlingFilter' class="table table-condensed table-bordered table-hover" style="width:100%">
                <thead>
                    <tr>
                        <th class="text-center">
                            <input id="selectall" type="checkbox"/>
                        </th>
                        <th class="text-center">Quantity Filter Name</th>
                        <th class="text-center">Shipping Handling Time</th>
                        <th class="text-center">New Product Quantity Status</th>
                        <th class="text-center">Product Count</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($handlingSetting))
                        @foreach($handlingSetting as $key => $value)
                            <tr>
                                <td width="5%" class="text-center">
                                    <input type="checkbox" id='chk_{{$key}}' name="product_id[]" value="{{$value['id']}}"/>
                                </td>
                                <td class="text-center">
                                    {{$platform}} quantity {{ $value['quantity_filter']." ".$value['quantity_filter_value'] }}
                                </td>
                                <Td class="text-center">
                                    <span class="badge bg-green"> {{ $value['shipping_value'] }} days </span>
                                    
                                </Td>
                                <td class="text-center">
                                    @if($value['force_product_new'] == 1)
                                        <span class="badge bg-green">Yes</span>
                                    @else
                                        <span class="badge bg-red">No</span>
                                    @endif
                                </td>
                                <td class="text-center">
                                    <span class="badge bg-green">{{$value['no_of_product']}}</span>
                                </td>
                                <td class="text-center">
                                    <a href='javascript:void(0);' data-type='Unblock Filter' data-where='id' data-blockId="{{$value['id']}}" data-vendor="{{$vendorId}}" class='btn btn-danger btn-xs removeHandlingFilter' data-toggle='tooltip' title='Blocked Product'><i class='fa fa-lg fa-trash'></i></a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        <div class="overlay" id="price_spin" style="display:none;">
            <i class="fa fa-spin fa-spinner">
            </i>
        </div>
    </div>
    @section('script')
        <script type="text/javascript">
            $(function() {
                handlingFilter = $('#handlingFilter').dataTable();
            });
        </script>
    @stop
@endif