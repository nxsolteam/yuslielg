<div class="box box-success">
    <div class="box-header with-border">
        <i aria-hidden="true" class="fa fa-search-plus">
        </i>
        <h3 class="box-title">
            Product Search
        </h3>
    </div>
    <div class="box-body">
        <form class="form-horizontal" id="frm_filter" name="frm_filter" role="form">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            Title :-
                        </label>
                        <div class="col-sm-8">
                            {!! Form::text('title',Input::old('title'),array('class'=>'form-control input-sm','placeholder'=>'Search By Product Title')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                        @if(isset($isBlock) && $isBlock == 'Yes')
                            Product / Distributor SKU :-
                        @else
                            Part No :-
                        @endif
                        </label>
                        <div class="col-sm-8">
                            {!! Form::text('product_id',Input::old('product_id'),array('class'=>'form-control input-sm','placeholder'=>'Search By ProductId / SKU')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            UPC Code :-
                        </label>
                        <div class="col-sm-8">
                            {!! Form::text('upc_code',Input::old('upc_code'),array('class'=>'form-control input-sm','placeholder'=>'Search By Product UPC Code')) !!}
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            Category :- 
                        </label>
                        <div class="col-sm-8">
                            {!! Form::select('category',[],Input::old('category'),array('id' => 'main_category','class'=>'form-control input-sm local_category_search')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            Distributor :- 
                        </label>
                        <div class="col-sm-8" id="divlocalchildcat">
                            {!! Form::select('vendor',$distributor,Input::old('vendor'),array('class'=>'form-control input-sm vendorChange select2')) !!}
                            <span class="help-inline text-danger" id="vendor_error"></span>
                        </div>
                    </div>
                </div>
                @if(isset($show) && $show == 'Yes')
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                            </label>
                            <div class="col-sm-8">
                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('zeroquantity') !!}
                                            Product with 0 quantity
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                            </label>
                            <div class="col-sm-8">
                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('zeroprice') !!}
                                            Products with quantity greater than zero
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if(isset($isQty) && $isQty == 'Yes')
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                            </label>
                            <div class="col-sm-8">
                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('zero_qty_greater') !!}
                                            Products with quantity > 0
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if(isset($asin) && $asin == 'Yes')
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                                ASIN :- 
                            </label>
                            <div class="col-sm-8" id="divlocalchildcat">
                                {!! Form::text('amz_productid',Input::old('amz_productid'),array('class'=>'form-control input-sm','placeholder'=>'Search By Product ASIN No')) !!}
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-md-offset-1">
                <button class="btn btn-sm btn-success" type="submit">
                    <i class="fa fa-search-plus" aria-hidden="true"></i> Search
                </button>
                <button class="btn btn-sm btn-default" id="btn_reset" type="reset">
                    <i class="fa fa-trash" aria-hidden="true"></i> Reset
                </button>
            </div>
        </form>
    </div>
    <div class="overlay" id="search_spin" style="display:none;">
        <i class="fa fa-spin fa-spinner">
        </i>
    </div>
</div>