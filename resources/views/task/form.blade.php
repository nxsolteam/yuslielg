<div class="form-group required">
	<label class="col-sm-2 control-label">Task Title :-</label>
	<div class="col-sm-6">
		{!! Form::text('name',Input::old('name'),array('class'=>'form-control','placeholder'=>'Task Name')) !!}
		{!! $errors->first('name','<span class="help-inline text-danger">:message</span>') !!}
	</div>
</div>

<div class="form-group required">
	<label class="col-sm-2 control-label">Task Description :-</label>
	<div class="col-sm-6">
		{!! Form::textarea('description',Input::old('description'),array('class'=>'form-control','placeholder'=>'Task Description')) !!}
		{!! $errors->first('description','<span class="help-inline text-danger">:message</span>') !!}
	</div>
</div>