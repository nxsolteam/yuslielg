<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title> New Uploaded Blocked Product Summary</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta charset="utf-8"/>
        <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
    </head>
    <body>
        <h3 class="box-title">Blocked Product List</h3>
        <table class="table table-condensed table-bordered table-hover" style="border: 1px solid #ccc; width: 100%;border-collapse: collapse;border-radius:10px" border="1">
            <thead>
                <tr style="background: #ccc;text-align: left;">
                    <th style="padding: 8px 10px;border: 1px solid #b9b7b7;">Product SKU</th>
                    <th style="padding: 8px 10px;border: 1px solid #b9b7b7;">Title</th>
                    <th style="padding: 8px 10px;border: 1px solid #b9b7b7;">Quantity</th>
                    <th style="padding: 8px 10px;border: 1px solid #b9b7b7;">Price</th>
                    <th style="padding: 8px 10px;border: 1px solid #b9b7b7;">Distributor</th>
                    @if(!empty($field))
                    @foreach($field as $value)
                    <th style="padding: 8px 10px;border: 1px solid #b9b7b7;">{{ucfirst($value)}}</th>
                    @endforeach
                    @endif
                </tr>
            </thead>
            <tbody>
                @if(!empty($final))
                @foreach($final as $value)
                    <tr>
                        <td width="10%" style="padding: 8px 10px;">
                            {{ $value['product_id'] }}
                        </td>
                        <td style="padding: 8px 10px;">
                            {{ $value['title'] }}
                        </td>
                        <td width="10%" style="padding: 8px 10px;">
                            {{ $value['quantity'] }}
                        </td>
                        <td style="padding: 8px 10px;">
                            {{ $value['unit_price'] }}
                        </td>
                        <td style="padding: 5px 10px;">
                            {{ $value['vendor'] }}
                        </td>
                        @if(!empty($field))
                        @foreach($field as $k1 => $row)
                        @if(isset($value[$k1]) && $value[$k1] == "1")
                        <td style="padding: 8px 10px;">
                            <?php $id = $value['id']; ?>
                            <a href='{{ route("$row.blockproduct.get",["id",$id]) }}'>
                                <span>Yes</span></a>
                            </a>
                        </td>
                        @else
                        <td style="padding: 8px 10px;">
                            <span>No</span></a>
                        </td>
                        @endif
                        @endforeach
                        @endif
                    </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="2" style="padding: 8px 10px;">
                        There are no block product record was found
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
    </body>
</html>