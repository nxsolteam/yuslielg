<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title> Price Summary</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    </head>
    <body>
        <table style="width:100%;border-collapse: collapse;border-radius:10px" border="1">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Product No</th>
                    <th>Unit Price</th>
                    <th>Quantity</th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($products))
                <?php $i=0; ?>
                @foreach($products as $value)
                <tr>
                    <td width="10%">
                        {{ ++$i }}
                    </td>
                    <td>
                        {{ $value->product_id }}
                    </td>
                    <td>
                        {{ $value->unit_price }}
                    </td>
                    <td>
                        {{ $value->quantity }}
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </body>
</html>