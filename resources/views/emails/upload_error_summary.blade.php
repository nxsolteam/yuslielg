<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> {{$batchName}} Error Summary</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
</head>
<body>
    <table style="width:100%">
    <thead>
        <tr>
            <th>Fail Count</th>
            <th>Fail Reason</th>
        </tr>
    </thead>
    <tbody>
        @foreach($summary as $value)
            <tr>
                <td>
                {{ $value['error_count'] }}
                </td>
                <td>
                {{ $value['error_reason'] }}
                </td>
            </tr>
        @endforeach
    </tbody>
    </table>
</body>
</html>
