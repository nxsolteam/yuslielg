<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Export Summary</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    {{ Html::style("assets/bootstrap/css/bootstrap.min.css") }}
    <style type="text/css">
        .bg-aqua {
            background-color: #00c0ef !important;
        }
        .bg-green {
            background-color: #00a65a !important;
        }
        .bg-red {
            background-color: #dd4b39 !important;
        }
    </style>
</head>
<body>
    <table id='logs' class="table table-condensed table-bordered table-hover" style="width:100%">
    <thead>
        <tr>
            <th>Type</th>
            <th>Uploaded Date</th>
            <th>Uploaded Time</th>
            <th>File Name</th>
            <th>Total Records</th>
            <th>Total Fail Records</th>
            <th>Success</th>
            <th>Downlod Error File</th>
        </tr>
    </thead>
    <tbody>
        @foreach($logs as $log)
            <tr>
                <td>{{ $log['type'] }}</td>
                <td>{{ $log['upload_date'] }}</td>
                <td>{{ $log['upload_time'] }}</td>
                <td>{{ $log['file'] }}</td>
                <td class="text-center"><span class="badge bg-aqua">{{ $log['total_record'] }}</span></td>
                <td class="text-center"><span class="badge bg-aqua">{{ $log['total_fail_record'] }}</span></td>
                <td class="text-center">
                    @if($log['success'] == 1)
                        <span class="badge bg-green">Yes</span>
                    @else
                        <span class="badge bg-red">No</span>
                    @endif
                </td>
                <td class="text-center">
                    @if($log['fail_file'] != NULL || $log['fail_file'] != '')
                        <?php
                            $path = asset('/uploads/'.$username);
                            $path = $path.'/errors/'.$log['fail_file'];
                        ?>
                        <a href='{{$path}}' data-toggle='tooltip' title='Download Error Data' data-placement='top' class='btn btn-sm btn-primary'>Download</a>
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
    </table>
</body>
</html>