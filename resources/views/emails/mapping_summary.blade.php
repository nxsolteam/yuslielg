<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
            <meta content="IE=edge" http-equiv="X-UA-Compatible">
                <title>
                    Mapping Summary
                </title>
                <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
                </meta>
            </meta>
        </meta>
    </head>
    <body>
        <table border="1" style="width:100%;border-collapse: collapse;border-radius:10px">
            <thead>
                <tr>
                    <td colspan="4" align="center">
                        <h3> Following is list of category where Mapping is Required </h3>
                    </td>
                </tr>
                <tr>
                    <th>
                        No
                    </th>
                    <th>
                        Category Name
                    </th>
                    <th>
                        Marketplace
                    </th>
                    <th>
                        Shopping Engine
                    </th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($final))
                    @foreach($final as $key => $value)
                        <tr>
                            <td width="10%" align="center">
                                {{ ($key + 1) }}
                            </td>
                            <td>
                                {{ $value['category'] }}
                            </td>
                            <td>
                                {{ $value['marketPlace'] }}
                            </td>
                            <td>
                                {{ $value['shoppingEngine'] }}
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </body>
</html>