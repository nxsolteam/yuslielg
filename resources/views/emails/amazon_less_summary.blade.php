<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
            <meta content="IE=edge" http-equiv="X-UA-Compatible">
                <title>
                    Error Summary
                </title>
                <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
                </meta>
            </meta>
        </meta>
    </head>
    <body>
        <h3> Following is list of amazon product where amazon price is incorrect </h3>
        <br>
        <table border="1" style="width:100%;border-collapse: collapse;border-radius:10px">
            <thead>
                <tr>
                    <th>
                        No
                    </th>
                    <th>
                        Product Id
                    </th>
                    <th>
                        Product Title
                    </th>
                    <th>
                        Amazon Quantity
                    </th>
                    <th>
                        Amazon Price
                    </th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($final))
                    @foreach($final as $key => $value)
                        <tr>
                            <td width="10%">
                                {{ $key + 1 }}
                            </td>
                            <td>
                                {{ $value['product_id'] }}
                            </td>
                            <td>
                                {{ $value['title'] }}
                            </td>
                            <td>
                                {{ $value['amazon_quantity'] }}
                            </td>
                            <td>
                                {{ $value['amazon_price'] }}
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </body>
</html>