<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title> Ebay Product</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    </head>
    <body>
        <table style="width:100%;border-collapse: collapse;border-radius:10px" border="1">
            <thead>
                <tr>
                    <th colspan="5">
                        {{$userName}}
                    </th>
                </tr>
                <tr>
                    <th>No</th>
                    <th>Product ID</th>
                    <th>Ebay ID</th>
                    <th>Product Title</th>
                    <th>Product Price</th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($final))
                    <?php $i=0; ?>
                    @foreach($final as $value)
                        <tr>
                            <td width="10%">
                                {{ ++$i }}
                            </td>
                            <td width="10%">
                                {{ $value['product_id'] }}
                            </td>
                            <td>
                                {{ $value['ebay_id'] }}
                            </td>
                            <td width="10%">
                                {{ $value['title'] }}
                            </td>
                            <td>
                                {{ $value['price'] }}
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <th colspan="5">
                            <h3><i>We have already update the ebay id in UMP</i></h3>
                        </th>
                    </tr>
                @else
                    <tr>
                        <td colspan="5">
                            There are no Ebay Duplicate Product Found
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>
    </body>
</html>