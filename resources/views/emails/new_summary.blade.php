<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title> New Product Summary</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    </head>
    <body>
        <h4> The following is list of new products that have been added for upload to Amazon. Please review their mapping and correct if necessary in the Amazon Product Category Mapping section.</h4> 
        <table style="width:100%;border-collapse: collapse;border-radius:10px" border="1">
            <thead>
                <tr>
                    <th>New Product Part #</th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($products))
                    @foreach($products as $value)
                        <tr>
                            <td width="10%">
                                {{ $value }}
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </body>
</html>