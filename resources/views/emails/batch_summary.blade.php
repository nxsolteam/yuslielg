<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title> {{$batchName}} Error Summary</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    </head>
    <body>
        <table style="width:100%;border-collapse: collapse;border-radius:10px" border="1">
            <thead>
                <tr>
                    <th>Fail Count</th>
                    <th>Fail Reason</th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($summary))
                    @foreach($summary as $value)
                        <tr>
                            <td width="10%">
                                {{ $value['error_count'] }}
                            </td>
                            <td>
                                {{ $value['error_reason'] }}
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="2">
                            There are no fail record was found
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>
    </body>
</html>