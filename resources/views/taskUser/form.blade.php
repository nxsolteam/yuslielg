<div class="form-group required">
	<label class="col-sm-2 control-label">First Name :-</label>
	<div class="col-sm-6">
		{!! Form::text('first_name',Input::old('first_name'),array('class'=>'form-control','placeholder'=>'First Name')) !!}
		{!! $errors->first('first_name','<span class="help-inline text-danger">:message</span>') !!}
	</div>
</div>

<div class="form-group required">
	<label class="col-sm-2 control-label">Last Name :-</label>
	<div class="col-sm-6">
		{!! Form::text('last_name',Input::old('last_name'),array('class'=>'form-control','placeholder'=>'Last Name')) !!}
		{!! $errors->first('last_name','<span class="help-inline text-danger">:message</span>') !!}
	</div>
</div>

<div class="form-group required">
	<label class="col-sm-2 control-label">Email :-</label>
	<div class="col-sm-6">
		{!! Form::text('email',Input::old('email'),array('class'=>'form-control','placeholder'=>'Email')) !!}
		{!! $errors->first('email','<span class="help-inline text-danger">:message</span>') !!}
	</div>
</div>

<div class="form-group required">
	<label class="col-sm-2 control-label">Mobile No :-</label>
	<div class="col-sm-6">
		{!! Form::text('mobile_no',Input::old('mobile_no'),array('class'=>'form-control numeric','placeholder'=>'Mobile No','maxlength' => '10')) !!}
		{!! $errors->first('mobile_no','<span class="help-inline text-danger">:message</span>') !!}
	</div>
</div>