@extends($layoutTheme)
@section('content')
<div class="box box-warning">
        <div class="box-header with-border">
            <i class="fa fa-unlock-alt" aria-hidden="true"></i>
            <h3 class="box-title">Change Password</h3>
        </div>
        {{ Form::open(array('route' => 'changepassword.post','class'=> 'form-horizontal')) }}
        <div class="box-body">
            <div class="form-group required">
                <label class="col-sm-2 control-label">Old Password :-</label>
                <div class="col-sm-5">
                    <input type="password" class="form-control" name="old_password" placeholder="Enter Old Passoword">
                    <span class="help-inline text-danger">{{ $errors->first('old_password') }}</span>
                </div>
            </div>
            <div class="form-group required">
                <label class="col-sm-2 control-label">New Password :-</label>
                <div class="col-sm-5">
                    <input type="password" class="form-control" name="password" placeholder="Enter New Passoword">
                    <span class="help-inline text-danger">{{ $errors->first('password') }}</span>
                </div>
            </div>
            <div class="form-group required">
                <label class="col-sm-2 control-label">Confirm Password :-</label>
                <div class="col-sm-5">
                    <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Passoword">
                    <span class="help-inline text-danger">{{ $errors->first('password_confirmation') }}</span>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-success">Update</button>
                    <button type="reset" class="btn btn-default">Cancel</button>
                </div>
            </div>
        </div>
        <!-- /.box-footer -->
        {{ Form::close() }}
        <!-- /.box-body -->
        <div id="spin" class="overlay" style="display:none;">
            <i class="fa fa-spin fa-spinner"></i>
        </div>
    </div>
    <!-- /.box -->
</div>

@stop
@section('script')
<script type="text/javascript">
$('form').submit(function(){
    myShow();
});
</script>
@stop