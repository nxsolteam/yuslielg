@extends($layoutTheme)
@section('content')
<div class="box box-warning">
	<div class="box-header with-border">
		<i class="fa fa-compass"></i>
		<h3 class="box-title">Create Group</h3>
		<div class="pull-right box-tools">
			<a class="btn btn-success btn-sm" data-toggle="tooltip" title="Back" href="{!!route('group.index')!!}"><i class="fa fa-lg fa fa-arrow-left"></i></a>
		</div>
		<!-- /. tools -->
	</div>
	<div class="box-body">
		<div class="row">
			{!! Form::open(array('url'=>route('group.store'),'class' => 'form-horizontal')) !!}
			@include("group.form")
		</div>
	</div>
	<div class="box-footer">
		<div class="form-group">
			<div class="col-sm-offset-2">
				<button type="submit" class="btn btn-success">
					<i class="fa fa-sliders" aria-hidden="true"></i> Save
				</button>
				<button type="reset" class="btn btn-default">
					<i class="fa fa-trash" aria-hidden="true"></i> Reset
				</button>
			</div>
		</div>
	</div>
	{!! Form::close() !!}
	<div id="spin" class="overlay" style="display:none;">
		<i class="fa fa-spin fa-spinner"></i>
	</div>
	<!-- /.box-body -->
</div>
@stop
@section('script')
<script type="text/javascript">
	$('form').submit(function(){
		myShow();
	});
</script>
@stop