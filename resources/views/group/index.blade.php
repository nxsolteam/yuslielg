@extends($layoutTheme)
@section('content')

<div class="box box-info">
    <div class="box-header with-border">
        <i class="fa fa-compass"></i>
        <h3 class="box-title">All Groups</h3>
        <div class="pull-right box-tools">
            <a class="btn btn-success btn-sm" id='add_product' data-toggle="tooltip" title="Add Groups" href="{{ route('group.create') }}"><i class="fa fa-fw fa-plus fa-lg"></i> </a>
        </div>
    </div>
    <div class="box-body">
        <table id='group' class="table table-condensed table-bordered table-hover" style="width:100%">
            <thead>
                <tr>
                    <th style="padding-left:20px;">No</th>
                    <th>Group</th>
                    <th>Action</th>
                </tr>
            </thead>
            <thead id="filters">
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div id="spin" class="overlay" style="display:none;">
        <i class="fa fa-spin fa-spinner"></i>
    </div>
    <!-- /.box-body -->
</div>
@stop

@section('script')

<script type="text/javascript">

$(function() {

    group = $('#group').dataTable({
        "bProcessing": false,
        "bServerSide": true,
        "bStateSave": true,
        "autoWidth": true,
        "sAjaxSource": "{{URL::route('group.index')}}",
        "sPaginationType": "listbox",
        "aaSorting": [
            [1, "asc"]
        ],
        "oLanguage": {
          "sLengthMenu": "Display _MENU_ records"
        },
        "aoColumns": [
        { "mData": "id",sWidth: "05%",},
        { "mData": "name",sWidth: "60%",},
        {
            mData: null,
            bSortable: false,
            sWidth: "5%",
            sClass: "text-center",
            mRender: function(v, t, o) {
                var editurl = '{{ route("group.edit", ":id") }}';
                editurl = editurl.replace(':id',o['id']);
                var act_html = "<a href='"+editurl+"' data-toggle='tooltip' title='Edit Group' data-placement='top' class='btn btn-primary btn-sm'><i class='fa fa-fw fa-edit fa-lg'></i></a>"
                                +"&nbsp"
                                +"<a href='javascript:void(0)' data-name='"+o['name']+"' data-id='"+o['id']+"'  data-type='group' data-toggle='tooltip' title='Delete Group' data-placement='top' class='btn btn-sm btn-danger btn-delete'><i class='fa fa-fw fa-trash-o fa-lg'></i></a>"
                return act_html;
            }
        }, ],
        fnPreDrawCallback: function() {
            myShow();
        },
        fnDrawCallback: function(oSettings) {
            myHide();
        },
        fnRowCallback : function(nRow, aData, iDisplayIndex){
           $("td:first", nRow).html(iDisplayIndex +1);
           return nRow;
        },
    });

    group.fnSetFilteringDelay(1000);

    $("#group thead#filters input:text").donetyping(function() {
        group.fnFilter(this.value, $("#group thead#filters input:text").index(this));
    });
});
</script>
@stop