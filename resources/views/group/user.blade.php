@extends($layoutTheme)
@section('content')
<div class="box box-warning">
	<div class="box-header with-border">
		<i class="fa fa-compass"></i>
		<h3 class="box-title">Assign User To Group</h3>
	</div>
	<div class="box-body">
		<div class="row">
			{!! Form::open(array('url'=>route('groupAssign'),'class' => 'form-horizontal')) !!}
			<div class="form-group required">
				<label class="col-sm-2 control-label">Group :-</label>
				<div class="col-sm-6">
					{!! Form::select('group_id',['' => '- Select Group -']+$groups,Input::get('group_id',null),array('class'=>'form-control select2','id' => 'group_id')) !!}
					{!! $errors->first('group_id','<span class="help-inline text-danger">:message</span>') !!}
				</div>
			</div>

			<div class="form-group required">
				<label class="col-sm-2 control-label">User :-</label>
				<div class="col-sm-6">
					{!! Form::select('user_id',$users,Input::get('user_id',null),array('class'=>'form-control select2','id' => 'user_id','multiple'=>'multiple','name'=> 'user_id[]')) !!}
					{!! $errors->first('name','<span class="help-inline text-danger">:message</span>') !!}
				</div>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<div class="form-group">
			<div class="col-sm-offset-2">
				<button type="submit" class="btn btn-success">
					<i class="fa fa-sliders" aria-hidden="true"></i> Save
				</button>
				<button type="reset" class="btn btn-default">
					<i class="fa fa-trash" aria-hidden="true"></i> Reset
				</button>
			</div>
		</div>
	</div>
	{!! Form::close() !!}
	<div id="spin" class="overlay" style="display:none;">
		<i class="fa fa-spin fa-spinner"></i>
	</div>
	<!-- /.box-body -->
</div>
@stop
@section('script')
<script type="text/javascript">
	$('form').submit(function(){
		myShow();
	});
</script>
@stop