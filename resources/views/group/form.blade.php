<div class="form-group required">
	<label class="col-sm-2 control-label">Group :-</label>
	<div class="col-sm-6">
		{!! Form::text('name',Input::old('name'),array('class'=>'form-control','placeholder'=>'Group Name')) !!}
		{!! $errors->first('name','<span class="help-inline text-danger">:message</span>') !!}
	</div>
</div>