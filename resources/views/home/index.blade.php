@extends($layoutTheme)
@section('content')
<style type="text/css">
  .home .fa {
    color: white;
  }
</style>
<div class="box box-success">
  <div class="box-header with-border">
    <i class="fa fa-home"></i>
    <h3 class="box-title">Welcome To To Do List</h3>
  </div>
  <div class="box-body home">
    <div class="col-lg-4 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>{{ number_format($groups) }}</h3>
          <p>Total Groups</p>
        </div>
        <div class="icon">
          <i class="fa fa-shopping-cart"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <div class="col-lg-4 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{ number_format($task)  }}</h3>
          <p>Total Task</p>
        </div>
        <div class="icon">
          <i class="fa fa-deaf" aria-hidden="true"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    
    <!-- ./col -->
    <div class="col-lg-4 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{ number_format($users) }}</h3>
          <p>Total Users</p>
        </div>
        <div class="icon">
          <i class="fa fa-image"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
  </div>
</div>
@stop
@section('script')

<script type="text/javascript">

</script>
@stop
