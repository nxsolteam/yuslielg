@extends(Config('myConfig.login_theme'))
@section('content')
<div class="login-box-body">
    <p class="login-box-msg">
        <i class="fa fa-fw fa-user">
        </i>
        Sign in to start your session
    </p>
    @include('partial.alert')
    <form role="form" method="POST" action="{{ url('/login') }}">
    {{ csrf_field() }}
    <div class="form-group has-feedback">
        <input class="form-control" id="username" name="email" placeholder="Your email address" type="text" value="{{ Input::old('email')}}"/>
        <span class="glyphicon glyphicon-envelope form-control-feedback">
        </span>
        <span class="text-danger">
            {{$errors->first('email')}}
        </span>
    </div>
    <div class="form-group has-feedback">
        <input class="form-control" id="password" name="password" placeholder="Your password" type="password">
        <span class="glyphicon glyphicon-lock form-control-feedback">
        </span>
        <span class="text-danger">
            {{$errors->first('password')}}
        </span>
        </input>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <button class="btn btn-success" id="signin" type="submit">
            Sign In
            </button>
        </div>
        <div class="col-xs-6">
            <label>
                <input type="checkbox">
                Remember Me
                </input>
            </label>
        </div>
    </div>
    {!! Form::close(); !!}
    <br>
    <div class="form-group">
        <div class="col-md-3">
            <a class="btn btn-link" href="{{ url('/password/reset') }}">
                Forgot Your Password?
            </a>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
    $('form').submit(function() {
        $('#signin').html('<i class="fa fa-lg fa-pulse fa-fw fa-spinner"></i> Signing in...').prop('disabled',true);
    });
</script>
@stop
