@extends(Config('myConfig.login_theme'))

@section('content')

<div class="login-box-body">
    @include('partial.alert')
    <p class="login-box-msg"><i class="fa fa-fw fa-user"></i> Reset Password</p>
    <form role="form" method="POST" action="{{ url('/password/reset') }}">
    {{ csrf_field() }}
    <input type="hidden" name="token" value="{{ $token }}">
        <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
            {{ Form::text('email',Input::old('email'), array('placeholder'=>'Email','class'=>'form-control')) }}
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            {!! $errors->first('email','<span class="help-inline text-danger">:message</span>'); !!}
        </div>
        <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
            {{ Form::password('password',array('placeholder' => 'Password','class' => 'form-control')) }}
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            {!! $errors->first('password','<span class="help-inline text-danger">:message</span>'); !!}
        </div>
        <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            {{ Form::password('password_confirmation',array('placeholder' => 'Confirm Password','class' => 'form-control')) }}
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            {!! $errors->first('password_confirmation','<span class="help-inline text-danger">:message</span>'); !!}
        </div>
        <div class="row">
            <!-- /.col -->
            <div class="col-xs-4">
                <button id="signin" type="submit" class="btn btn-success">Submit</button>
            </div>
            <!-- /.col -->
        </div>
    </form>
</div>
@endsection
