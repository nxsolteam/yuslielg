@extends(Config('myConfig.login_theme'))

<!-- Main Content -->
@section('content')

<!-- /.login-logo -->
<div class="login-box-body">
    @include('partial.alert')
    <form role="form" method="POST" action="{{ url('/password/email') }}">
    {{ csrf_field() }}
    <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
        <input class="form-control" id="username" name="email" placeholder="Your email address" type="text" value="{{ Input::old('email')}}"/>
        <span class="glyphicon glyphicon-envelope form-control-feedback">
        </span>
        <span class="text-danger">
            {{$errors->first('email')}}
        </span>
    </div>

    <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
            <button id="signin" type="submit" class="btn btn-success">Submit</button>
        </div>
        <!-- /.col -->
        <!-- /.col -->
        <div class="pull-right" style="margin-right:12px">
            <a href="{{route('login')}}" class="btn btn-primary">Back</a>
        </div>
        <!-- /.col -->
    </div>
    </form>
</div>
<!-- /.login-box-body -->
@endsection
