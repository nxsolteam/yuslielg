<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
        <title>
            {{Config('app.name')}}| Log in
        </title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
        {{ Html::style('assets/bootstrap/css/bootstrap.min.css') }}
        {{ Html::style("assets/plugins/font-awesome/css/font-awesome.min.css") }}
        {{ Html::style('assets/dist/css/AdminLTE.min.css') }}
        @section("styles")
        @show
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="#">
                    <b>
                        To Do
                    </b>
                    List
                </a>
            </div>
            @yield('content')
        </div>
        <!-- Scripts -->
        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
        {{ Html::script("assets/plugins/jQuery/jquery-2.2.3.min.js") }}
        {{ Html::script('assets/bootstrap/js/bootstrap.min.js') }}
        @section("scripts")
        @show
    </body>
</html>
