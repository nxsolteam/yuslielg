<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return redirect()->route('login');
});

Route::group(['prefix' => 'api'], function () {
    Route::get('group', [
        'as'   => 'group',
        'uses' => 'APIController@getGroup',
    ]);
    Route::get('task/{group}', [
        'as'   => 'task',
        'uses' => 'APIController@getTaskByGroupName',
    ]);
    Route::get('task/{taskId}/{status}', [
        'as'   => 'task',
        'uses' => 'APIController@updateStatus',
    ]);
});
Route::group(['middleware' => ['adminauth']], function () {
	Route::get('dashboard', [
        'as'   => 'dashboard',
        'uses' => 'DashboardController@index',
    ]);

    Route::group(['prefix' => 'change'], function () {
        Route::get('password', array(
            'as'   => 'changepassword.get',
            'uses' => 'UserController@getChangePassword',
        ));

        Route::post('password', array(
            'as'   => 'changepassword.post',
            'uses' => 'UserController@postChangePassword',
        ));
    });

    //Group routes start
    Route::group(['prefix' => 'group'], function () {
        Route::delete('delete', [
            'as'   => 'group.delete',
            'uses' => 'GroupController@delete',
        ]);

        Route::get('taskAssign', [
            'as'   => 'taskAssign',
            'uses' => 'GroupController@taskAssign',
        ]);
    });
    Route::resource('group', 'GroupController', ['except' => 'destroy', 'show']);
    //Group routes end
    
    //Task routes start
    Route::group(['prefix' => 'task'], function () {
        Route::delete('delete', [
            'as'   => 'task.delete',
            'uses' => 'TaskController@delete',
        ]);
    });
    Route::resource('task', 'TaskController', ['except' => 'destroy', 'show']);
    //Task routes end
    
    //Task User routes start
    Route::group(['prefix' => 'taskUser'], function () {
        Route::delete('delete', [
            'as'   => 'taskUser.delete',
            'uses' => 'TaskUserController@delete',
        ]);
    });
    Route::resource('taskUser', 'TaskUserController', ['except' => 'destroy', 'show']);
    //Task User routes end
    
    Route::get('taskAssign', [
        'as'   => 'taskAssign',
        'uses' => 'GroupController@taskAssign',
    ]);

    Route::post('taskAssign', [
        'as'   => 'taskAssign',
        'uses' => 'GroupController@taskAssignStore',
    ]);

    Route::get('groupAssign', [
        'as'   => 'groupAssign',
        'uses' => 'GroupController@groupAssign',
    ]);

    Route::post('groupAssign', [
        'as'   => 'groupAssign',
        'uses' => 'GroupController@groupAssignStore',
    ]);

    Route::get('logout', [
        'as'   => 'logout',
        'uses' => 'Auth\LoginController@logout',
    ]);
});
