<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\GroupTask;
use App\Model\Task;
use App\Model\TaskGroup;
use Illuminate\Http\Request;

class APIController extends Controller
{
    /**
     * [getGroup description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getGroup(Request $request)
    {
        $allGroup = TaskGroup::pluck("name")->all();
        $final    = [];
        foreach ($allGroup as $key => $value) {
            $final[]['group_name'] = $value;
        }
        return response()->json($final, 200);
    }

    /**
     * [getTaskByGroupName description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getTaskByGroupName(Request $request, $groupName)
    {
        $task    = [];
        $groupId = TaskGroup::where("name", $groupName)->pluck("id")->all();
        if (!empty($groupId)) {
            $allGroup = GroupTask::select("task.id","task.name", "task.description")->leftJoin("task", "task.id", "group_task.task_id")->whereIn("group_task.group_id", $groupId)->where("task.status", "0")->get()->all();
        }
        $final = [];
        $i=0;
        foreach ($allGroup as $key => $value) {
            $final[$i]['task_no']      = $value['id'];
            $final[$i]['task_name']    = $value['name'];
            $final[$i]['task_details'] = $value['description'];
            $i++;
        }
        return response()->json($final, 200);
    }

    /**
     * [updateStatus description]
     * @param  Request $request  [description]
     * @param  [type]  $taskName [description]
     * @param  [type]  $status   [description]
     * @return [type]            [description]
     */
    public function updateStatus(Request $request, $taskId, $status)
    {
        Task::Where("id", $taskId)->update(['status' => $status, 'updated_at' => date("Y-m-d H:i:s")]);
    }
}
