<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * [__construct description]
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (Auth::check()) {
                $this->userId   = Auth::user()->id;
                $this->userName = Auth::user()->name;
                $this->user     = Auth::user();
                $this->share();
            }
            return $next($request);
        });
    }

    /**
     * [share description]
     * @return [type] [description]
     */
    public function share()
    {
        View::share('userId', $this->userId);
        View::share('userName', $this->userName);
        View::share('user', $this->user);
        View::share('layoutTheme', Config("myConfig.theme"));
        View::share('projectName', Config("myConfig.project_name"));
        View::share('title', "");
    }
}
