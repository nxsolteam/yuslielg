<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\GroupTask;
use App\Model\Task;
use App\Model\TaskGroup;
use App\Model\TaskUser;
use App\Model\UserGroup;
use Illuminate\Http\Request;
use Validator;
use View;

class GroupController extends Controller
{
    /**
     * [__construct description]
     */
    public function __construct()
    {
        parent::__construct();
        $this->viewPath = "group";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        View::share("title", "Group");

        if ($request->ajax()) {
            $whereStr    = '1 = ?';
            $whereParams = [1];

            if ($request->has('sSearch')) {
                $search = trim(trim($request->get('sSearch')));
                $whereStr .= " and ( name like '%{$search}%'"
                    . ")";
            }

            $columns = ['task_group.id', 'task_group.name'];

            $group = TaskGroup::select($columns)
                ->whereRaw($whereStr, $whereParams)
                ->groupBy("task_group.id");

            $groupCcount = TaskGroup::select('id')
                ->whereRaw($whereStr, $whereParams)
                ->count();

            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $group = $group->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                $sql_order = '';
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $group = $group->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }

            $group = $group->get();

            $response['iTotalDisplayRecords'] = $groupCcount;
            $response['iTotalRecords']        = $groupCcount;
            $response['sEcho']                = intval($request->get('sEcho'));
            $response['aaData']               = $group;
            return $response;
        }

        return view($this->viewPath . '.index');
    }

    /**
     * [create description]
     * @return [type] [description]
     */
    public function create()
    {
        View::share("title", "Group");
        return view($this->viewPath . '.create');
    }

    /**
     * [store description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
        $data  = $request->all();
        $rules = [
            'name' => "required|unique:task_group,name",
        ];
        $message = [
            'name.unique' => 'The group must be unique',
        ];
        $validator = Validator::make($data, $rules, $message);
        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator)
                ->with('message_type', 'danger')
                ->with('message', 'There were some error try again');
        }

        TaskGroup::insert([
            'name'       => $request->name,
            "created_at" => date("Y-m-d H:i:s"),
            "updated_at" => date("Y-m-d H:i:s"),
        ]);

        return back()->with('message_type', 'success')
            ->with('message', 'Group Store Successfully');
    }

    /**
     * [edit description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function edit($id)
    {
        View::share("title", "Group");
        $group = TaskGroup::where('id', $id)->first();
        return view($this->viewPath . '.edit', compact('group'));
    }

    /**
     * [update description]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function update(Request $request, $id)
    {
        $data  = $request->all();
        $rules = [
            'name' => "required|unique:task_group,name,$id,id",
        ];
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator)
                ->with('message_type', 'danger')
                ->with('message', 'There were some error try again');
        }
        TaskGroup::where('id', $id)->update(['name' => $request->name]);
        return back()->with('message_type', 'success')
            ->with('message', 'Group Updated Successfully');
    }

    /**
     * [delete description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function delete(Request $request)
    {
        View::share("title", "Group");
        $ids = $request->id;
        if (!is_array($ids)) {
            $ids = explode(",", $ids);
        }
        GroupTask::whereIn('group_id', $ids)->delete();
        TaskGroup::whereIn('id', $ids)->delete();
        return response()->json(['success' => true], 200);
    }

    /**
     * [taskAssign description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function taskAssign(Request $request)
    {
        View::share("title", "Assing Task To Group");
        $groups = TaskGroup::pluck("name", "id")->all();
        $tasks  = Task::pluck("name", "id")->all();
        return view($this->viewPath . '.assign', compact('groups', 'tasks'));
    }

    /**
     * [taskAssignStore description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function taskAssignStore(Request $request)
    {
        $data = $request->all();

        $rules = [
            'group_id' => "required",
            'task_id'  => "required",
        ];
        $message = [
            'group_id.required' => 'This fields is required',
            'task_id.unique'    => 'This fields is required',
        ];
        $validator = Validator::make($data, $rules, $message);
        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator)
                ->with('message_type', 'danger')
                ->with('message', 'There were some error try again');
        }

        $allTask = GroupTask::where("group_id", $request->group_id)->pluck("task_id")->all();

        $final = [];
        foreach ($data['task_id'] as $key => $value) {
            if (!in_array($value, $allTask)) {
                $final[$key]['group_id']   = $data['group_id'];
                $final[$key]['task_id']    = $value;
                $final[$key]['created_at'] = date("Y-m-d H:i:s");
                $final[$key]['updated_at'] = date("Y-m-d H:i:s");
            }
        }

        if (!empty($final)) {
            foreach (array_chunk($final, 500) as $key => $insertRow) {
                GroupTask::insert($insertRow);
            }
        }

        return back()->with('message_type', 'success')
            ->with('message', 'Group Task Updated Successfully');
    }

    /**
     * [groupAssign description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function groupAssign(Request $request)
    {
        View::share("title", "Assing Group To User");
        $groups = TaskGroup::pluck("name", "id")->all();
        $users  = TaskUser::pluck("first_name", "id")->all();
        return view($this->viewPath . '.user', compact('groups', 'users'));
    }

    /**
     * [groupAssignStore description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function groupAssignStore(Request $request)
    {
        $data = $request->all();

        $rules = [
            'group_id' => "required",
            'user_id'  => "required",
        ];
        $message = [
            'group_id.required' => 'This fields is required',
            'user_id.unique'    => 'This fields is required',
        ];
        $validator = Validator::make($data, $rules, $message);
        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator)
                ->with('message_type', 'danger')
                ->with('message', 'There were some error try again');
        }

        $allTask = UserGroup::where("group_id", $request->group_id)->pluck("user_id")->all();

        $final = [];
        foreach ($data['user_id'] as $key => $value) {
            if (!in_array($value, $allTask)) {
                $final[$key]['group_id']   = $data['group_id'];
                $final[$key]['user_id']    = $value;
                $final[$key]['created_at'] = date("Y-m-d H:i:s");
                $final[$key]['updated_at'] = date("Y-m-d H:i:s");
            }
        }

        if (!empty($final)) {
            foreach (array_chunk($final, 500) as $key => $insertRow) {
                UserGroup::insert($insertRow);
            }
        }

        return back()->with('message_type', 'success')
            ->with('message', 'Group Task Updated Successfully');
    }
}
