<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\User;
use Auth;
use Illuminate\Http\Request;
use Redirect;
use Validator;
use View;

class UserController extends Controller
{
    /**
     * [getChangePassword description]
     * @return [type] [description]
     */
    public function getChangePassword(Request $request)
    {
        View::share("title", "Change Password");
        return View('user.changepassword');
    }

    /**
     * [postChangePassword description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postChangePassword(Request $request)
    {
        $id   = Auth::user()->id;
        $data = $request->all();

        Validator::extend('check_password', function ($attribute, $value, $parameters) {
            $user         = User::find(Auth::user()->id);
            $old_password = \Hash::check($value, $user->password);
            if ($old_password == true) {
                return true;
            } else {
                return false;
            }
        });

        $rules = [
            'old_password'          => 'required|check_password',
            'password'              => 'confirmed|required|min:8',
            'password_confirmation' => 'required',
        ];

        $messages = [
            'password.confirmed'             => 'password not match',
            'old_password.required'          => 'this field is required',
            'password.required'              => 'this field is required',
            'password_confirmation.required' => 'this field is required',
            'old_password.check_password'    => 'wrong password',
        ];

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {

            return Redirect::back()->withErrors($validator->errors())
                ->with('message', 'Some thinks is going wrong.')
                ->with('message_type', 'danger');
        }

        $user           = User::find($id);
        $user->Password = \hash::make($request->password);
        $user->fill($data);
        $user->save();

        return Redirect::back()->with('message', 'Password Change Successfully.')
            ->with('message_type', 'success');
    }
}
