<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Task;
use Illuminate\Http\Request;
use Validator;
use View;

class TaskController extends Controller
{
    /**
     * [__construct description]
     */
    public function __construct()
    {
        parent::__construct();
        $this->viewPath = "task";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        View::share("title", "Task");

        if ($request->ajax()) {
            $whereStr    = '1 = ?';
            $whereParams = [1];

            if ($request->has('sSearch')) {
                $search = trim(trim($request->get('sSearch')));
                $whereStr .= " and ( name like '%{$search}%'"
                    . ")";
            }

            $columns = ['task.id as id', 'task.name','task.description','status'];

            $task = Task::select($columns)
                ->whereRaw($whereStr, $whereParams)
                ->groupBy("task.id");

            $taskCount = Task::select('id')
                ->whereRaw($whereStr, $whereParams)
                ->count();

            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $task = $task->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                $sql_order = '';
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $task = $task->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }

            $task = $task->get();

            $response['iTotalDisplayRecords'] = $taskCount;
            $response['iTotalRecords']        = $taskCount;
            $response['sEcho']                = intval($request->get('sEcho'));
            $response['aaData']               = $task;
            return $response;
        }

        return view($this->viewPath . '.index');
    }

    /**
     * [create description]
     * @return [type] [description]
     */
    public function create()
    {
        View::share("title", "Task");
        return view($this->viewPath . '.create');
    }

    /**
     * [store description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
        $data  = $request->all();
        $rules = [
            'name'        => "required|unique:task,name",
            'description' => "required",
        ];
        $message = [
            'name.unique' => 'The task must be unique',
        ];
        $validator = Validator::make($data, $rules, $message);
        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator)
                ->with('message_type', 'danger')
                ->with('message', 'There were some error try again');
        }

        Task::insert([
            'name'        => $request->name,
            'description' => $request->description,
            "created_at"  => date("Y-m-d H:i:s"),
            "updated_at"  => date("Y-m-d H:i:s"),
        ]);

        return back()->with('message_type', 'success')
            ->with('message', 'Task Store Successfully');
    }

    /**
     * [edit description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function edit($id)
    {
        View::share("title", "Task");
        $task = Task::where('id', $id)->first();
        return view($this->viewPath . '.edit', compact('task'));
    }

    /**
     * [update description]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function update(Request $request, $id)
    {
        $data  = $request->all();
        $rules = [
            'name'        => "required|unique:task,name,$id,id",
            'description' => "required",
        ];
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator)
                ->with('message_type', 'danger')
                ->with('message', 'There were some error try again');
        }
        Task::where('id', $id)->update(['name' => $request->name, 'description' => $request->description, 'updated_at' => date("Y-m-d H:i:s")]);
        return back()->with('message_type', 'success')
            ->with('message', 'Task Updated Successfully');
    }

    /**
     * [delete description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function delete(Request $request)
    {
        View::share("title", "Task");
        $ids = $request->id;
        if (!is_array($ids)) {
            $ids = explode(",", $ids);
        }
        Task::whereIn('id', $ids)->delete();
        return response()->json(['success' => true], 200);
    }
}
