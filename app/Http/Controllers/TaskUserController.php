<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\TaskUser;
use Illuminate\Http\Request;
use Validator;
use View;

class TaskUserController extends Controller
{
    /**
     * [__construct description]
     */
    public function __construct()
    {
        parent::__construct();
        $this->viewPath = "taskUser";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        View::share("title", "User");

        if ($request->ajax()) {
            $whereStr    = '1 = ?';
            $whereParams = [1];

            if ($request->has('sSearch')) {
                $search = trim(trim($request->get('sSearch')));
                $whereStr .= " and ( first_name like '%{$search}%'"
                    . ")";
            }

            $columns = ['id', 'first_name', 'last_name', 'email', 'mobile_no'];

            $taskUser = TaskUser::select($columns)
                ->whereRaw($whereStr, $whereParams)
                ->groupBy("task_user.id");

            $taskUserCount = TaskUser::select('id')
                ->whereRaw($whereStr, $whereParams)
                ->count();

            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $taskUser = $taskUser->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                $sql_order = '';
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $taskUser = $taskUser->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }

            $taskUser = $taskUser->get();

            $response['iTotalDisplayRecords'] = $taskUserCount;
            $response['iTotalRecords']        = $taskUserCount;
            $response['sEcho']                = intval($request->get('sEcho'));
            $response['aaData']               = $taskUser;
            return $response;
        }

        return view($this->viewPath . '.index');
    }

    /**
     * [create description]
     * @return [type] [description]
     */
    public function create()
    {
        View::share("title", "User");
        return view($this->viewPath . '.create');
    }

    /**
     * [store description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
        $data  = $request->all();
        $rules = [
            'first_name' => "required|unique:task_user,first_name",
            'last_name'  => "required",
            'email'      => "required|email|unique:task_user,email",
            'mobile_no'  => "required|digits:10",
        ];
        $message = [
            'email.unique'      => 'The task user email must be unique',
            'first_name.unique' => 'The task user first name must be unique',
        ];
        $validator = Validator::make($data, $rules, $message);
        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator)
                ->with('message_type', 'danger')
                ->with('message', 'There were some error try again');
        }
        
        TaskUser::insert([
            'first_name' => $request->first_name,
            'last_name'  => $request->last_name,
            'email'      => $request->email,
            'mobile_no'  => $request->mobile_no,
            "created_at" => date("Y-m-d H:i:s"),
            "updated_at" => date("Y-m-d H:i:s"),
        ]);

        return back()->with('message_type', 'success')
            ->with('message', 'TaskUser Store Successfully');
    }

    /**
     * [edit description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function edit($id)
    {
        View::share("title", "User");
        $taskUser = TaskUser::where('id', $id)->first();
        return view($this->viewPath . '.edit', compact('taskUser'));
    }

    /**
     * [update description]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function update(Request $request, $id)
    {
        $data  = $request->all();

        $rules = [
            'first_name' => "required|unique:task_user,first_name,id,$id",
            'last_name'  => "required",
            'email'      => "required|email|unique:task_user,email,id,$id",
            'mobile_no'  => "required|digits:10",
        ];
        $message = [
            'email.unique'      => 'The task user email must be unique',
            'first_name.unique' => 'The task user first name must be unique',
        ];
        $validator = Validator::make($data, $rules, $message);
        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator)
                ->with('message_type', 'danger')
                ->with('message', 'There were some error try again');
        }

        $rules = [
            'name' => "required",
        ];
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return back()->withInput()
                ->withErrors($validator)
                ->with('message_type', 'danger')
                ->with('message', 'There were some error try again');
        }
        TaskUser::where('id', $id)->update([
             'first_name' => $request->first_name,
            'last_name'  => $request->last_name,
            'email'      => $request->email,
            'mobile_no'  => $request->mobile_no,
            "updated_at" => date("Y-m-d H:i:s"),
        ]);
        return back()->with('message_type', 'success')
            ->with('message', 'TaskUser Updated Successfully');
    }

    /**
     * [delete description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function delete(Request $request)
    {
        View::share("title", "User");
        $ids = $request->id;
        if (!is_array($ids)) {
            $ids = explode(",", $ids);
        }
        TaskUser::whereIn('id', $ids)->delete();
        return response()->json(['success' => true], 200);
    }
}
