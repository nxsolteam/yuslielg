<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use View;
use App\Model\TaskGroup;
use App\Model\Task;
use App\Model\TaskUser;

class DashboardController extends Controller
{
    /**
     * [__construct description]
     */
    public function __construct()
    {
        parent::__construct();
        $this->viewPath = "home";
    }

    /**
     * [index description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function index(Request $request)
    {
        View::share("title", "Dashboard");

        $groups = TaskGroup::count();
        $task = Task::count();
        $users = TaskUser::count();
        return view($this->viewPath . '.index', compact('groups', 'task','users'));
    }
}
