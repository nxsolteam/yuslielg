<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    protected $table = "user_group";

    protected $fillable = ['group_id','user_id'];

    public $timestamps = true;
}
