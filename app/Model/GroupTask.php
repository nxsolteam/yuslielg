<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GroupTask extends Model
{
    protected $table = "group_task";

    protected $fillable = ['group_id','task_id'];

    public $timestamps = true;
}
