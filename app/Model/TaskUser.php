<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TaskUser extends Model
{
    protected $table = "task_user";

    protected $fillable = ['name'];

    public $timestamps = true;
}
