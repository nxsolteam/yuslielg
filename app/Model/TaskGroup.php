<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TaskGroup extends Model
{
    protected $table = "task_group";

    protected $fillable = ['name'];

    public $timestamps = true;
}
