<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Model\Product;
use App\Utility\DatabaseUtility;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ProductMetaJob extends Job
{
    use DispatchesJobs;
    /**
     * Create a new job instance.
     */
    public $sheets;
    public $imagePath;
    public $logger;
    public $changeAll;

    public function __construct($sheets, $userId)
    {
        parent::__construct($userId);
        $this->sheets = $sheets;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        echo "Script Start :-" . date("Y-m-d H:i:s") . "\n";

        $sheets = $this->sheets;

        $userName = $this->userName;
        $userId   = $this->userId;

        ini_set('memory_limit', '2048M');
        set_time_limit(0);

        $totalRecords = count($sheets);

        echo "Total No Of products $totalRecords \n";

        $sheetData = $productIds = $zeroQtyProductIds = [];

        foreach ($sheets as $key => $sheet) {
            if (isset($sheet['dallas_qty']) && ($sheet['dallas_qty'] == 0 || $sheet['dallas_qty'] < 0)) {
                $zeroDallsQtyProductIds[] = $sheet['product_id'];
            }

            if ($sheet['quantity'] == 0 || $sheet['quantity'] < 0) {
                $zeroQtyProductIds[] = $sheet['product_id'];
            } else if (isset($sheet['product_id']) && !empty($sheet['product_id']) && isset($sheet['quantity']) && isset($sheet['unit_price'])) {
                $productIds[]                                  = $sheet['product_id'];
                $sheetData[$sheet['product_id']]['product_id'] = $sheet['product_id'];
                $sheetData[$sheet['product_id']]['quantity']   = $sheet['quantity'];
                $sheetData[$sheet['product_id']]['weight']     = str_replace(',', '', number_format($sheet['weight'], 2));
                $sheetData[$sheet['product_id']]['width']      = str_replace(',', '', number_format($sheet['width'], 2));
                $sheetData[$sheet['product_id']]['length']     = str_replace(',', '', number_format($sheet['length'], 2));
                $sheetData[$sheet['product_id']]['height']     = str_replace(',', '', number_format($sheet['height'], 2));
                $sheetData[$sheet['product_id']]['unit_price'] = str_replace(',', '', number_format($sheet['unit_price'], 2));
                $sheetData[$sheet['product_id']]['unit_cost']  = str_replace(',', '', number_format($sheet['cost'], 2));
                $sheetData[$sheet['product_id']]['vendor']     = $sheet['vendor'];
                $sheetData[$sheet['product_id']]['vendor_sku'] = $sheet['vendor_sku'];
                $sheetData[$sheet['product_id']]['dallas_qty'] = "0";
                if ($userId == "1" && isset($sheet['dallas_qty'])) {
                    $sheetData[$sheet['product_id']]['dallas_qty'] = $sheet['dallas_qty'];
                } 
            }
        }


        if (!empty($zeroQtyProductIds)) {
            
            echo "\n=======================================\n";
            echo "Out Of Stock Query Start :-" . date("Y-m-d H:i:s") . "\n";
            echo "Total No Of out of stock products " . count($zeroQtyProductIds) . "\n";

            $update               = [];
            $update['quantity']   = "0";
            $update["updated_at"] = date("Y-m-d H:i:s");
            
            if($userId == '1') {
                $update['dallas_qty'] = "0";
            }

            if (isset($this->userRoles) && in_array('1', $this->userRoles)) {
                $update['ebay_quantity'] = "0";
            }

            if (isset($this->userRoles) && in_array('2', $this->userRoles)) {
                $update['amazon_quantity'] = "0";
            }

            if (isset($this->userRoles) && in_array('10', $this->userRoles)) {
                $update['walmart_quantity'] = "0";
            }

            foreach (array_chunk($zeroQtyProductIds, 500) as $key => $productList) {
                Product::where("user_id", $userId)->whereRaw("quantity != '0'")->whereIn("product_id", $productList)
                    ->update($update);
            }
        }

        echo "Out Of Stock Query End :-" . date("Y-m-d H:i:s") . "\n";
        echo "\n=======================================\n";

        if (!empty($zeroDallsQtyProductIds) && $userId=='1') {
            
            echo "Dallas Quantity Out Of Stock Query Start :-" . date("Y-m-d H:i:s") . "\n";
            echo "Dallas Quantity Total No Of out of stock products " . count($zeroDallsQtyProductIds) . "\n";

            $update               = [];
            $update['dallas_qty']   = "0";
            $update["updated_at"] = date("Y-m-d H:i:s");

            foreach (array_chunk($zeroDallsQtyProductIds, 500) as $key => $productList) {
                Product::where("user_id", $userId)->whereRaw("dallas_qty != '0'")->whereIn("product_id", $productList)
                    ->update($update);
            }
            echo "Dallas Quantity Out Of Stock Query End :-" . date("Y-m-d H:i:s") . "\n";
            echo "\n=======================================\n";
        }


        $fields = [
            "id", "product_id", "quantity", "unit_price", "unit_cost", "weight", "width", "height", "length", "vendor"
            , "vendor_sku", "dallas_qty",
        ];

        $userRoles = DatabaseUtility::getUserRoles($userId);

        $platforms = [
            '1'  => 'ebay',
            '2'  => 'amazon',
            '6'  => 'rakuten',
            '10' => 'walmart',
        ];

        foreach ($platforms as $key => $value) {
            if (isset($userRoles) && in_array("$key", $userRoles)) {
                foreach (['_price_status', '_price_value', '_shipping_price'] as $key => $row) {
                    $fields[] = $value . $row;
                }
            }
        }

        $productsData = [];
        $final        = [];

        foreach (array_chunk($productIds, 500) as $key1 => $productId) {
            $products = Product::select($fields)->where("user_id", $userId)->whereIn('product_id', $productId)->get();
            if (!empty($products)) {
                foreach ($products as $key => $product) {
                    $final[$product->product_id]                      = $product->toArray();
                    $productsData[$product->product_id]['product_id'] = $product->product_id;
                    $productsData[$product->product_id]['quantity']   = $product->quantity;
                    $productsData[$product->product_id]['dallas_qty'] = $product->dallas_qty;
                    $productsData[$product->product_id]['unit_price'] = str_replace(',', '', number_format($product->unit_price, 2));
                    $productsData[$product->product_id]['unit_cost']  = str_replace(',', '', number_format($product->unit_cost, 2));
                    $productsData[$product->product_id]['weight']     = str_replace(',', '', number_format($product->weight, 2));
                    $productsData[$product->product_id]['width']      = str_replace(',', '', number_format($product->width, 2));
                    $productsData[$product->product_id]['height']     = str_replace(',', '', number_format($product->height, 2));
                    $productsData[$product->product_id]['length']     = str_replace(',', '', number_format($product->length, 2));
                    $productsData[$product->product_id]['vendor']     = $product->vendor;
                    $productsData[$product->product_id]['vendor_sku'] = $product->vendor_sku;
                }
            }
        }

        foreach ($sheetData as $key => $sheet) {
            if (isset($productsData[$key]) && !empty($productsData[$key])) {
                if ($productsData[$key]['unit_price'] == $sheet['unit_price'] &&
                    $productsData[$key]['quantity'] == $sheet['quantity'] &&
                    $productsData[$key]['dallas_qty'] == $sheet['dallas_qty'] &&
                    $productsData[$key]['weight'] == $sheet['weight'] &&
                    $productsData[$key]['length'] == $sheet['length'] &&
                    $productsData[$key]['width'] == $sheet['width'] &&
                    $productsData[$key]['height'] == $sheet['height'] &&
                    $productsData[$key]['unit_cost'] == $sheet['unit_cost'] &&
                    $productsData[$key]['vendor'] == $sheet['vendor'] &&
                    $productsData[$key]['vendor_sku'] == $sheet['vendor_sku']) {
                    unset($sheetData[$key]);
                    unset($productsData[$key]);
                    unset($final[$key]);
                } else {
                    if ($productsData[$key]['unit_price'] == $sheet['unit_price'] &&
                        $productsData[$key]['quantity'] == $sheet['quantity'] &&
                        $productsData[$key]['weight'] == $sheet['weight'] &&
                        $productsData[$key]['vendor'] == $sheet['vendor'] &&
                        $productsData[$key]['vendor_sku'] == $sheet['vendor_sku']) {
                        unset($sheetData[$key]['vendor']);
                        unset($sheetData[$key]['vendor_sku']);
                        unset($sheetData[$key]['quantity']);
                        unset($sheetData[$key]['weight']);
                        unset($sheetData[$key]['unit_price']);
                    }

                    if ($productsData[$key]['quantity'] == $sheet['quantity']) {
                        unset($sheetData[$key]['quantity']);
                    }
                    if ($productsData[$key]['dallas_qty'] == $sheet['dallas_qty']) {
                        unset($sheetData[$key]['dallas_qty']);
                    }
                    if ($productsData[$key]['unit_cost'] == $sheet['unit_cost']) {
                        unset($sheetData[$key]['unit_cost']);
                    }
                    if ($productsData[$key]['weight'] == $sheet['weight'] && $productsData[$key]['unit_price'] == $sheet['unit_price']) {
                        unset($sheetData[$key]['weight']);
                        unset($sheetData[$key]['unit_price']);
                    }
                    if ($productsData[$key]['width'] == $sheet['width']) {
                        unset($sheetData[$key]['width']);
                    }
                    if ($productsData[$key]['height'] == $sheet['height']) {
                        unset($sheetData[$key]['height']);
                    }
                    if ($productsData[$key]['length'] == $sheet['length']) {
                        unset($sheetData[$key]['length']);
                    }
                    if ($productsData[$key]['vendor_sku'] == $sheet['vendor_sku']) {
                        unset($sheetData[$key]['vendor_sku']);
                    }
                    if ($productsData[$key]['product_id'] == $sheet['product_id']) {
                        unset($sheetData[$key]['product_id']);
                    }
                }
            }
        }

        $ebayUpdatedId = $amzUpdatedId = $walmartUpdatedId = [];

        $preMeta = DatabaseUtility::getPreMetaData($userId);

        echo "Meta Update Query Start :-" . date("Y-m-d H:i:s") . "\n";

        $query     = [];
        $statement = null;

        $quantityData = $lengthData = $widthData = $weightData = $heightData = $productMeta = $dallsQuantityData= [];

        $unitCostData = [];

        echo "\n=======================================\n";
        echo "\nRecord Need to Be Updated :- " . count($sheetData) . "\n";

        foreach ($sheetData as $key => $product) {
            if (isset($final[$key]) && !empty($final[$key])) {
                //quantity
                if (isset($product['quantity']) && !empty($product['quantity'])) {
                    $quantityData[$product['quantity']][] = $key;
                    unset($product['quantity']);
                }

                //dalls quantity
                if (isset($product['dallas_qty'])) {
                    $dallsQuantityData[$product['dallas_qty']][] = $key;
                    unset($product['dallas_qty']);
                }

                //unit cost
                if (isset($product['unit_cost']) && !empty($product['unit_cost'])) {
                    $unitCostData[$product['unit_cost']][] = $key;
                    unset($product['unit_cost']);
                }

                //length
                if (isset($product['length']) && !empty($product['length'])) {
                    $lengthData[$product['length']][] = $key;
                    unset($product['length']);
                }

                //height
                if (isset($product['height']) && !empty($product['height'])) {
                    $heightData[$product['height']][] = $key;
                    unset($product['height']);
                }

                //width
                if (isset($product['width']) && !empty($product['width'])) {
                    $widthData[$product['width']][] = $key;
                    unset($product['width']);
                }

                //weight
                if (isset($product['weight']) && !empty($product['weight'])) {
                    $weightData[$product['weight']][] = $key;
                }

                // product meta
                if (!empty($product['unit_price']) && isset($product['unit_price']) && $product['unit_price'] > 0) {
                    $product = DatabaseUtility::preMetaData($userId, $product, $preMeta, $final[$key]);
                }

                if(!empty($product) && isset($product)) {
                    $productMeta[$key] = $product;
                }
            } 
            unset($sheetData[$key]);
        }

        //length
        if(!empty($lengthData) && isset($lengthData)) {
            echo "\n=======================================\n";
            echo "\nTotal length Record Need to Be Updated :- " . count($lengthData) . "\n";
            $tot=1;
            foreach ($lengthData as $k => $lengthValue) {
                foreach (array_chunk($lengthValue, 1000) as $key => $length) {
                    $statement = "";
                    $statement = "UPDATE products SET `length`='".$k."',`updated_at`='".date("Y-m-d H:i:s")."' WHERE `user_id`=$userId AND `product_id` IN('" . implode("','", $length) . "')";
                    DB::statement($statement);
                    /*Product::where("user_id", $userId)->whereIn("product_id", $length)->update([
                        'length'     => $k,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]);*/
                }
                echo "\n-------------------------------------\n";
                echo "$tot :- Update length $k in ".count($lengthValue). " Records out of ".count($lengthData)." at time ".date("Y-m-d H:i:s");
                $tot++;
            }
        }

        //height
        if(!empty($heightData) && isset($heightData)) {
            echo "\n=======================================\n";
            echo "\nTotal height Record Need to Be Updated :- " . count($heightData) . "\n";
            $tot=1;
            foreach ($heightData as $k => $heightValue) {
                foreach (array_chunk($heightValue, 1000) as $key => $height) {
                    $statement = "";
                    $statement = "UPDATE products SET `height`='".$k."',`updated_at`='".date("Y-m-d H:i:s")."' WHERE `user_id`=$userId AND `product_id` IN('" . implode("','", $height) . "')";
                    DB::statement($statement);
                    /*Product::where("user_id", $userId)->whereIn("product_id", $height)->update([
                        'height'     => $k,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]);*/
                }
                echo "\n-------------------------------------\n";
                echo "$tot :- Update height $k in ".count($heightValue). " Records out of ".count($heightData)." at time ".date("Y-m-d H:i:s");
                $tot++;
            }
        }

        //width
        if(!empty($widthData) && isset($widthData)) {
            echo "\n=======================================\n";
            echo "\nTotal width Record Need to Be Updated :- " . count($widthData) . "\n";
            $tot=1;
            foreach ($widthData as $k => $widthValue) {
                foreach (array_chunk($widthValue, 500) as $key => $width) {
                    $statement = "";
                    $statement = "UPDATE products SET `width`='".$k."',`updated_at`='".date("Y-m-d H:i:s")."' WHERE `user_id`=$userId AND `product_id` IN('" . implode("','", $width) . "')";
                    DB::statement($statement);
                    /*Product::where("user_id", $userId)->whereIn("product_id", $width)->update([
                        'width'     => $k,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]);*/
                }
                echo "\n-------------------------------------\n";
                echo "$tot :- Update width $k in ".count($widthValue). " Records out of ".count($widthData)." at time ".date("Y-m-d H:i:s");
                $tot++;
            }
        }

        //weight
        if(!empty($weightData) && isset($weightData)) {
            echo "\n=======================================\n";
            echo "\nTotal weight Record Need to Be Updated :- " . count($weightData) . "\n";
            $tot=1;
            foreach ($weightData as $k => $weightValue) {
                foreach (array_chunk($weightValue, 500) as $key => $weight) {
                    $statement = "";
                    $statement = "UPDATE products SET `weight`='".$k."',`updated_at`='".date("Y-m-d H:i:s")."' WHERE `user_id`=$userId AND `product_id` IN('" . implode("','", $weight) . "')";
                    DB::statement($statement);
                    /*Product::where("user_id", $userId)->whereIn("product_id", $weight)->update([
                        'weight'     => $k,
                    ]);*/
                }
                echo "\n-------------------------------------\n";
                echo "$tot :- Update weight $k in ".count($weightValue). " Records out of ".count($weightData)." at time ".date("Y-m-d H:i:s");
                $tot++;
            }
        }

        //quantity
        if(!empty($quantityData) && isset($quantityData)) {
            echo "\n=======================================\n";
            echo "\nTotal Quantity Record Need to Be Updated :- " . count($quantityData) . "\n";
            $tot=1;
            foreach ($quantityData as $k => $quantityValue) {
                $ebayUpdatedId = $amzUpdatedId = $walmartUpdatedId = $quantityValue;
                foreach (array_chunk($quantityValue, 500) as $key => $quantity) {
                    $statement = "";
                    $statement = "UPDATE products SET `quantity`='".$k."',`ebay_quantity`='".$k."',`amazon_quantity`='".$k."',`walmart_quantity`='".$k."',`updated_at`='".date("Y-m-d H:i:s")."' WHERE `user_id`=$userId AND `product_id` IN('" . implode("','", $quantity) . "')";
                    DB::statement($statement);
                    /*Product::where("user_id", $userId)->whereIn("product_id", $quantity)->update([
                        'quantity'         => $k,
                        'ebay_quantity'    => $k,
                        'amazon_quantity'  => $k,
                        'walmart_quantity' => $k,
                        'updated_at'       => date("Y-m-d H:i:s"),
                    ]);*/
                }
                echo "\n-------------------------------------\n";
                echo "$tot :- Update quantity $k in ".count($quantityValue). " Records out of ".count($quantityData)." at time ".date("Y-m-d H:i:s");
                $tot++;
            }
        }

        //Dalls quantity
        if(!empty($dallsQuantityData) && isset($dallsQuantityData)) {
            echo "\n=======================================\n";
            echo "\nTotal Dalss Quantity Record Need to Be Updated :- " . count($dallsQuantityData) . "\n";
            $tot=1;
            foreach ($dallsQuantityData as $k => $dallsQuantityValue) {
                foreach (array_chunk($dallsQuantityValue, 500) as $key => $dallsQuantity) {
                    $statement = "";
                    $statement = "UPDATE products SET `dallas_qty`='".$k."',`updated_at`='".date("Y-m-d H:i:s")."' WHERE `user_id`=$userId AND `product_id` IN('" . implode("','", $dallsQuantity) . "')";
                    DB::statement($statement);
                    /*Product::where("user_id", $userId)->whereIn("product_id", $dallsQuantity)->update([
                        'dallas_qty'         => $k,
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]);*/
                }
                echo "\n-------------------------------------\n";
                echo "$tot :- Update dallas quantity $k in ".count($dallsQuantityValue). " Records out of ".count($dallsQuantityData)." at time ".date("Y-m-d H:i:s");
                $tot++;
            }
        }

        //unit Cost
        if(!empty($unitCostData) && isset($unitCostData)) {
            echo "\n=======================================\n";
            echo "\nTotal Unit Cost Record Need to Be Updated :- " . count($unitCostData) . "\n";
            $tot=1;
            foreach ($unitCostData as $k => $unitCostValue) {
                foreach (array_chunk($unitCostValue, 500) as $key => $unitCost) {
                    $statement = "";
                    $statement = "UPDATE products SET `unit_cost`='".$k."',`updated_at`='".date("Y-m-d H:i:s")."' WHERE `user_id`=$userId AND `product_id` IN('" . implode("','", $unitCost) . "')";
                    DB::statement($statement);
                    /*Product::where("user_id", $userId)->whereIn("product_id", $quantity)->update([
                        'unit_cost'         => $k,
                        'updated_at'       => date("Y-m-d H:i:s"),
                    ]);*/
                }
                echo "\n-------------------------------------\n";
                echo "$tot :- Update unit cost $k in ".count($unitCostValue). " Records out of ".count($unitCostData)." at time ".date("Y-m-d H:i:s");
                $tot++;
            }
        }

        //meta
        if(!empty($productMeta) && isset($productMeta)) {

            $ebayUpdatedId = $amzUpdatedId = $walmartUpdatedId = array_keys($productMeta);
            echo "\n=======================================\n";
            echo "\nTotal Record Need to Be Updated :- " . count($productMeta) . "\n";

            DatabaseUtility::sendMail($userName . " Total Record Need to Be Updated :- " . count($productMeta), " Total Record Need to Be Updated For $userName:- " . count($productMeta));
            $tot=1;
            $finalQuery = [];
            foreach ($productMeta as $key => $product) {
                $statement = "";
                $statement = "UPDATE products SET ";
                foreach ($product as $k => $val) {
                    $statement .= "`$k` ='" . $val . "',";
                }
                $statement .= "`updated_at`='".date("Y-m-d H:i:s")."' WHERE `user_id`=$userId AND `product_id`=$key;";

                $finalQuery[$key] = $statement;

                /*DB::statement($statement);

                Product::where('product_id', $key)
                    ->where('user_id', "$userId")
                    ->update($product);
                echo "\n-------------------------------------\n";
                echo "$tot :- Update out of ".count($productMeta)." at time ".date("Y-m-d H:i:s");
                $tot++;*/
            }

            if(!empty($finalQuery)) {
                $tot=1;
                foreach ($finalQuery as $key => $query) {
                    DB::statement($query);
                    echo "\n-------------------------------------\n";
                    echo "$tot :- Update out of ".count($finalQuery)." at time ".date("Y-m-d H:i:s");
                    $tot++;
                }
            }
        }

        echo "Meta Update Query End :-" . date("Y-m-d H:i:s");

        echo "\n=======================================\n";

        if (isset($this->userRoles) && in_array('1', $this->userRoles) && !empty($ebayUpdatedId)) {
            DatabaseUtility::ebayStatus($userId, $ebayUpdatedId, 'product_id');
        }

        if (isset($this->userRoles) && in_array('2', $this->userRoles) && !empty($amzUpdatedId)) {
            DatabaseUtility::amazonStatus($userId, $amzUpdatedId, 'product_id');
        }

        if (isset($this->userRoles) && in_array('10', $this->userRoles) && !empty($walmartUpdatedId)) {
            DatabaseUtility::walmartStatus($userId, $walmartUpdatedId, 'product_id');
        }

        DB::statement("UPDATE `products` set parent_id='0' where parent_id is null");

        DatabaseUtility::vendorCount($userId);

        $sheetcount = count($sheetData);

        if ($sheetcount > 0) {
            $errorPath = public_path("uploads/$userName/errors");
            $excelName = $userName . '_excel_' . sha1(microtime());
            Excel::create($excelName, function ($excel) use ($sheetData) {
                $excel->sheet('Database Excel', function ($sheet) use ($sheetData) {
                    $data_excel = [];
                    foreach (current($sheetData) as $key => $value) {
                        $data_excel[] = $key;
                        $sheet->row(1, $data_excel);
                    }
                    $sheet->fromArray($sheetData, null, 'A2', false, false);
                    $sheet->row(1, function ($row) {
                        $row->setFontWeight('bold');
                    });
                });
            })->save('xlsx', $errorPath);
            return ['success' => false, 'excel_path' => $excelName . '.xlsx', 'totalRecords' => $totalRecords, 'totalFailRecords' => $sheetcount];
        } else {
            return ['success' => true, 'totalRecords' => $totalRecords];
        }
        return ['success' => true, 'totalRecords' => $totalRecords];
    }
}
