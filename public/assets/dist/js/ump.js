jQuery(document).ready(function() {
    //basic
    var slideToTop = jQuery("<div />");
    slideToTop.html('<i class="fa fa-chevron-up"></i>');
    slideToTop.css({
        position: 'fixed',
        bottom: '20px',
        right: '25px',
        width: '40px',
        height: '40px',
        color: '#eee',
        'font-size': '',
        'line-height': '40px',
        'text-align': 'center',
        'background-color': '#222d32',
        cursor: 'pointer',
        'border-radius': '5px',
        'z-index': '99999',
        opacity: '.7',
        'display': 'none'
    });
    slideToTop.on('mouseenter', function() {
        jQuery(this).css('opacity', '1');
    });
    slideToTop.on('mouseout', function() {
        jQuery(this).css('opacity', '.7');
    });
    jQuery('.wrapper').append(slideToTop);
    jQuery(window).scroll(function() {
        if (jQuery(window).scrollTop() >= 150) {
            if (!jQuery(slideToTop).is(':visible')) {
                jQuery(slideToTop).fadeIn(500);
            }
        } else {
            jQuery(slideToTop).fadeOut(500);
        }
    });
    jQuery(slideToTop).click(function() {
        jQuery("body").animate({
            scrollTop: 0
        }, 500);
    });
    jQuery(".sidebar-menu li:not(.treeview) a").click(function() {
        var jQuerythis = jQuery(this);
        var target = jQuerythis.attr("href");
        if (typeof target === 'string') {
            jQuery("body").animate({
                scrollTop: (jQuery(target).offset().top) + "px"
            }, 500);
        }
    });
    jQuery(".dateTimePicker").datetimepicker({
        datepicker: true,
        format: 'm/d/Y H:i',
        step: 5
    });
    jQuery.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="_token"]').attr('content')
        }
    });
    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.replace(new RegExp(search, 'g'), replacement);
    };
    jQuery.ajaxSetup({
        statusCode: {
            401: function() {
                swal({
                    title: "Session Timeout",
                    type: "error",
                    timer: 2000,
                    showConfirmButton: false
                });
                location.reload();
            }
        }
    });
    
    jQuery(".select2").select2();
    
    /* ----------------------------------------------------------------------- */
    //validation for numeric value 
    jQuery(".numeric").keydown(function(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    
    /**
     * 
     * @param  {[type]} ) {                   deleteId [description]
     * @return {[type]}   [description]
     */
    jQuery(document).on("click", ".btn-delete", function() {
        deleteId = jQuery(this).data('id');
        type = jQuery(this).data('type');
        name = jQuery(this).data('name');
        existCheck = jQuery(this).data('exist');
        productCount = jQuery(this).data('count');
        if (deleteId == undefined) {
            var deleteElement = jQuery('#' + type + ' tbody input[type=checkbox]:checked');
            var deleteId = [];
            jQuery.each(deleteElement, function(i, ele) {
                deleteId.push(jQuery(ele).val());
            });
        } else {
            deleteId = [deleteId];
        }
        if (0 == deleteId) {
            swal({
                title: "please select a record to proceed",
                type: "warning",
                timer: 3000,
                showConfirmButton: true
            });
            return false;
        } else {
            jQuery('#confirmDelete .modal-title').html('Delete ' + type);
            jQuery('#confirmDelete .modal-body p').html('Are you sure you want to delete this ' + name + " " + type + ' ?');
            jQuery('#confirmDelete .model_click').attr('data-value', deleteId);
            jQuery('#confirmDelete .model_click').attr('data-type', type);
            jQuery('#confirmDelete .model_click').attr('data-exist', existCheck);
            jQuery('#confirmDelete .model_click').attr('data-productCount', productCount);
            jQuery('#confirmDelete .model_click').addClass('model_click_master');
            jQuery('#confirmDelete').modal('show');
        }
    });
    
    /**
     * [description]
     * @param  {[type]} ) {               } [description]
     * @return {[type]}   [description]
     */
    jQuery(document).on("click", ".model_click_master", function() {
        var deleteId = jQuery(this).attr('data-value');
        var type = jQuery(this).attr('data-type');
        jQuery.ajax({
            url: public_path + type + "/delete",
            type: 'delete',
            dataType: 'json',
            data: {
                id: deleteId,
            },
            beforeSend: function() {
                myShow();
                jQuery('#replaceConfirm').modal('hide');
                jQuery('#confirmDelete').modal('hide');
            },
            complete: function() {
                myHide();
                var redrawtable = jQuery('#' + type).dataTable();
                redrawtable.fnStandingRedraw();
            },
            success: function(respObj) {
                jQuery('#messsage').find("p").removeClass("callout-danger callout-success");
                if (respObj.success) {
                    jQuery('#messsage').find("p").addClass('callout callout-success').html(type + " Deleted Successfully .").show().delay(3000).fadeOut('slow');
                } else {
                    jQuery('#messsage').find("p").addClass('callout callout-danger').html(respObj.message).show().delay(5000).fadeOut('slow');
                }
                $("html, body").animate({
                    scrollTop: 0
                }, "slow");
            },
        });
    });
});
/**
 * [myShow description]
 * @return {[type]} [description]
 */
function myShow() {
    $('#spin').show();
    $('#arrange_spin').show();
    $('#modal_spin').show();
    $('#search_spin').show();
    $('#price_spin').show();
    $('#price_arran_spin').show();
    $('#shipping_arran_spin').show();
    $('#quantity_spin').show();
    $('#shipping_spin').show();
    $('#block_spin').show();
}
/**
 * [myHide description]
 * @return {[type]} [description]
 */
function myHide() {
    $('#spin').hide();
    $('#arrange_spin').hide();
    $('#modal_spin').hide();
    $('#search_spin').hide();
    $('#price_spin').hide();
    $('#price_arran_spin').hide();
    $('#shipping_arran_spin').hide();
    $('#quantity_spin').hide();
    $('#block_spin').hide();
    $('#shipping_spin').hide();
}